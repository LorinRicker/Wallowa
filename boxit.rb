#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# boxit.rb
#
# Copyright © 2020-2023 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free4 Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

# Use:  $ boxit [--options] box-diagram-source-file

# A box-diagrammer/pretty-printer which transforms "simple" box-diagram layouts
# (easily edited) into proper ANSI-terminal line-drawing (SO/SI) renditions
# (which are near-impossible to edit).  In other words, transforms this:
#
#    +------+
#    |      |  into -->    (show sample)
#    |      |
#    +------+              Given/Assert: There are no hidden (i.e, Control
#                                        or Escape-Sequence) characters in
#                                        the simple box text representation.                                representation
#
# The tricky part is determining which correct corner character (upper-left,
# upper-right, lower-left, lower-right) to choose/substitute for each '+'...
# See the description of corner-scoring (evaluation) and the StrutMap lookup
# table below.
#
# Primary use case:  KED text editor (VMS/TPU) keymap diagrams for PuTTY and other
#                    (e.g., Reflection) VT1xx/ANSI-compliant terminal emulators.
#

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v2.0 (04/09/2023)"
  AUTHOR = "Lorin Ricker, Valley Center, California, USA"

OUTFILEEXT      = "box"
StartBoxesToken = "[StartBoxes]".downcase
EndBoxesToken   = "[EndBoxes]".downcase

# -----

require 'optparse'
require_relative 'lib/BoxIt'
require_relative 'lib/CLU'
include CLU

# ----------

def optparse( options )
  OptionParser.new { |opts|
    # --- Character set option ---
    opts.on( "-c", "--charset[=LDCHARSET]", /ANSI|UTF8/i,
            "Line drawing character set: ANSI, or UTF8 (default)" ) do |val|
      options[:charset] = ( val || "UTF8" ).downcase.to_sym
    end  # -c --charset
    # --- Page-type option ---
    opts.on( "-p", "--page[=TYPE]", /FLAT|WRAP/i,
            "Page type: WRAP or FLAT (default)" ) do |val|
      options[:page] = ( val || "FLAT" ).downcase.to_sym
    end  # -p --page
    opts.on( "-i", "--visible",
            "Substitute visible characters for invisible SI & SO",
            "characters (for diagram debugging)" ) do |val|
      options[:visible] = true
    end  # -i --visible

    opts.separator ""
    opts.on( "-n", "--noop", "--dryrun", "--test",
            "Dry-run (test & display, no-op) mode" ) do |val|
      options[:noop]  = true
      options[:verbose] = true  # Dry-run implies verbose...
    end  # -n --noop
    # --- Verbose option ---
    opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
      options[:verbose] = true
    end  # -v --verbose
    # --- Debug option ---
  opts.on( "-D", "--debug", "=DebugLevel", Integer,
           "Show debug information (levels: 1, 2 or 3)",
           "  1 - enable basic debug information output",
           "  2 - enable advanced debug information output",
           "  3 - enable (starts) 'Ruby debug' gem debugger" ) do |val|
    options[:debug] = val.to_i
  end  # -D --debug
  opts.on( "-P", "--pry", "Debug with pry (rather than 'Ruby debug' gem)") do |val|
    options[:prydebug] = true
    options[:debug] = DBGLVL3
  end  # -P --pry
    # --- About option ---
    opts.on_tail( "-a", "--about", "Display program info" ) do |val|
      require_relative 'lib/AboutProgram'
      options[:about] = about_program( PROGID, AUTHOR, true )
    end  # -a --about
    # --- Set the banner & Help option ---
    opts.banner = "\n  Usage: #{PROGNAME} [options] simple_box_file [finished_output_file]" +
                  "\n\n    where \"simple_box_file\" is a text file containing a simple box drawing (specification)," +
                  "\n    and \"finished_output_file\" is the finished line-drawn output file.\n" +
                  "\n    Default output file name is defaulted from the filename of the input \"simple_box_file\"," +
                  "\n    and the file type will be either \".ansibox\" or \".utf8box\", based on charset.\n\n"
    opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
      $stdout.puts opts
      # $stdout.puts "«+»Additional Text«+»"
      options[:help] = true
      exit true
    end  # -? --help
  }.parse!  # leave residue-args in ARGV
end  # optparse

# === Main ===

# Turn on/off Config-File option (default is off)
# options.merge!( { :configfile => false } )  # change to true to enable...
# Add-in Common Options ( --help, --about, --dryrun, --sudo,
#                         --verbose, --debug, --pry )
# options.merge!( CLU.common_options )

options = { :charset   => :utf8,   # ANSI|UTF8
            :page      => :flat,   # FLAT|WRAP
            :visible   => false,
            :noop      => false,
            :verbose   => false,
            :debug     => CLU::DBGLVL0,
            :prydebug  => false,
            :about     => false
          }

optparse( options )
CLU.debuggerRequired?( options[:debug], options[:prydebug] )
pp options if options[:debug] >= CLU::DBGLVL3

# Unusual:  Prior to OptionParsing, locate and open box-diagram source file,
#           collect any preamble --options into ARGV for subsequent processing.
# ----------
# The following is UNUSUAL in that it plucks the input filespec out of the
# ARGV array *prior to* OptionParse'ing -- this is so we can read through
# the file to recognize and add any internal --options to ARGV, and *then*
# OptionParse it.  Any internal --options are added to ARGV such that a com-
# line --option *supersedes* any matching internal --option.
# Locate the user's input text file from com-line args, open and read its
# preamble lines, including comments and internal --options, stop reading
# at StartBoxesToken ... If no input file, display help text.

simpletext = Array.new

do_preamble = false
argcount = ARGV.count
if ( argcount == 0 )
  ARGV.insert( 0, '--help' )  # for OptionParse processing... Display help text!
  optparse( options )
else
  # Find the box-diagram source file (if specified) in ARGV, as precursor
  # to opening it for preamble processing... Leave any com-line --options
  # in ARGV for OptionParser processing below.
  if inputfspec = ARGV.each.find { |e| e[0] != '-' ? e : nil }
    inputfspec = File.expand_path( inputfspec )
    do_preamble = true
  else
    # if no source file given on com-line, display help text --
    ARGV.insert( 0, '--help' ) unless ARGV[0] == "--about" || ARGV[0] == "-a"
    optparse( options )
  end
end
if do_preamble
  maxlinelen = 0  # for padding short simpletext when all are read...
  begin
    File.open( inputfspec, "r" ) do | srcf |
      readingtext = false  # expect "preamble lines" up-to and until StartBoxesToken
      lcount = 0
      while line = srcf.gets
        line = readingtext ? line.chomp : line.chomp.strip
        break if ( line == EndBoxesToken )  # will also break on EOF...
        lcount += 1
        readingtext = ( line.downcase == StartBoxesToken ) || readingtext
        if readingtext
          simpletext << line if ( line.downcase != StartBoxesToken ) \
                             && ( line.downcase != EndBoxesToken )
          maxlinelen = line.length if line.length > maxlinelen
        else
          case line[0]
          when '#'  # a comment, just skip it...
            next
          when '-'  # an internal option, add it to ARGV:
            ARGV.insert( 0, line )
          else
            STDERR.puts "%#{PROGNAME}-w-unkl, unrecognized preamble line: '#{line}'" if line.length > 0
            # otherwise, just skip empty lines...
          end  # case
        end  # if
      end  # while
    end
  rescue Errno::ENOENT => e
    STDERR.puts "%#{PROGNAME}-e-fnf, error opening input file '#{inputfspec}'"
    exit false
  end
  # Pad all simpletext to the same length:
  simpletext.each { | line | line << ' ' * ( maxlinelen - line.length ) }
end
# End of UNUSUAL stuff...
# At this point, simpletext contains all of the simple-box source text lines...

# if File.basename( inputfspec.to_s ) != File.basename( ARGV[0] )
#   STDERR.puts "%#{PROGNAME}-e-argerror, first-argument mismatch: #{ARGV[0]}"
#   exit false
# end

# if ARGV[1] nil, default to inputfspec name:
ARGV[1] ||= File.basename( inputfspec, ".*" )
outputfspec = ARGV[1]
if ( File.extname( outputfspec ) == "" )
  defext = "." + options[:charset].to_s + OUTFILEEXT
  outputfspec = File.expand_path( outputfspec + defext )
end

# Create the instance 'transform' --
transform = BoxIt::LineGraphics.new( simpletext, options )

transform.to_linedrawn

transform.print( outputfspec )

exit true
