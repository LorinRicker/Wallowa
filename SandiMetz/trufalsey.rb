#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# trufalsey.rb
#
# Copyright © 2019 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

# This is Sandi Metz's "Nothing is Something" monkey-patch on
# TrueClass, FalseClass and NilClass, demonstrating that you can
# make true/false evaluations strictly with methods (functions),
# completely without if-then-else constructs.
#
# See Sandi's excellent talk "Nothing is Something" (RailsConf 2015) here:
#     https://www.youtube.com/watch?v=OMPfEXIlTVE

class Object   # promoted from TrueClass

  def if_true
    yield
    self
  end

  def if_false
    self
  end

end  # TrueClass

class FalseClass

  def if_true
    self
  end

  def if_false
    yield
    self
  end

end  # FalseClass

class NilClass

  def if_true
    self
  end

  def if_false
    yield
    self
  end

end  # FalseClass


# demostrations:

( 1 == 1 ).if_true { puts "evaluated true" }
# => evaluated true

( 1 == 1 ).if_false { puts "evaluated false" }
# => block is ignored

( 1 == 2 ).if_true { puts "evaluated true" }
# => block is ignored

( 1 == 2 ).if_false { puts "evaluated false" }
# => evaluated false

# in fact, this acts exactly as Ruby wants:

"anything".if_true { puts "anything is true" }
# => anything is true

"anything".if_false { puts "anything is not false" }
# => block is ignored

nil.if_true { puts "nil is not true" }
# => block is ignored

nil.if_false { puts "nil is actually falsey" }
# => nil is actually falsey

# now, could actually replace this:
if ( 1 == 1 )
  puts "is true"
else
  puts "is false"
end

# with this:
( 1 == 1 ).
  if_true { puts "is true" }.
  if_false { puts "is false" }

# => is true

# and this:
if ( 1 == 2 )
  puts "is true"
else
  puts "is false"
end

# with this:
( 1 == 2 ).
  if_true { puts "is true" }.
  if_false { puts "is false" }

# => is false

# ...So, really, we can do boolean evaluations
#    simply by passing messages (functions),
#    and we don't really need if-then-else
