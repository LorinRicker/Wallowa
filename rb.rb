#!/usr/bin/env ruby

# from https://github.com/thisredone/rb (GPL)
# v1.1 02/28/2020 - Updated to include .rbrc load
#                   and improved main-line code
# v1.2 03/02/2024 - minor fix: exists? -> exist? (Ruby v3.3)

File.join(Dir.home, '.rbrc').tap { |f| load f if File.exist?(f) }

def execute(_, code)
  puts _.instance_eval(&code)
rescue Errno::EPIPE
  exit
end

single_line = ARGV.delete('-l')
code = eval("Proc.new { #{ARGV.join(' ')} }")
single_line ? STDIN.each { |l| execute(l.chomp, code) } : execute(STDIN.each_line, code)

exit true

# Examples (from the GitHub repository's README.md) --
# Extract docker images from running containers
#
# > docker ps | rb drop 1 | rb -l split[1]
#
# # ubuntu
# # postgres
#
# Display how much time ago containers have exited
#
# > docker ps -a | rb grep /Exited/ | rb -l 'split.last.ljust(20) + " => " + split(/ {2,}/)[-2]'
#
# # angry_hamilton      => Exited (0) 18 hours ago
# # dreamy_lamport      => Exited (0) 3 days ago
# # prickly_hypatia     => Exited (0) 2 weeks ago
#
# Sort df -h output by Use%
#
# > df -h | rb 'drop(1).sort_by { |l| l.split[-2].to_f }'
#
# # udev                         3,9G     0  3,9G   0% /dev
# # tmpfs                        3,9G     0  3,9G   0% /sys/fs/cgroup
# # /dev/sda1                    511M  3,4M  508M   1% /boot/efi
# # /dev/sda2                    237M   85M  140M  38% /boot
#
# # or leave the header if you want
# > df -h | rb '[first].concat drop(1).sort_by { |l| l.split[-2].to_f }'
#
# # Filesystem                   Size  Used Avail Use% Mounted on
# # udev                         3,9G     0  3,9G   0% /dev
# # tmpfs                        3,9G     0  3,9G   0% /sys/fs/cgroup
# # /dev/sda1                    511M  3,4M  508M   1% /boot/efi
# # /dev/sda2                    237M   85M  140M  38% /boot
#
# Count files by their extension
#
# > find . -type f | rb -l File.extname self | rb 'group_by(&:itself).map { |ext, o| "#{ext.chomp}: #{o.size}" }'
#
# # : 3
# # .rb: 19
# # .md: 1
