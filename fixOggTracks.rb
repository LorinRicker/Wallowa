#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# fixOggTracks.rb

# Use:
# $ cd $rips
# $ fixOggTracks [debug]

# Copyright © 2021-23 Lorin Ricker <Lorin@RickerNet.us>
# Version 2.0, 07/03/2023

##########################################################################################
# This version is for target:
#     49 - Stevenson - Passacaglia on DSCH - Pars Prima - Sonata allegro.ogg
# --> 01 - Pars Prima - Sonata allegro.ogg  (...etc.)
##########################################################################################

# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.

# PROGNAME = "filerename"
#   PROGID = "#{PROGNAME} v2.0 (07/03/2023)"
#   AUTHOR = "Lorin Ricker, Valley Center, California, USA"

require 'fileutils'

EMPTYSTR = ''.freeze

direntries = Dir.glob( "*.ogg", File::FNM_DOTMATCH )
# Remove the back- and self- directory links
direntries.delete_if { | e | File.basename( e ) == '.' ||
                             File.basename( e ) == '..'}

tracknr = "01"
direntries.each do | fspec |
  # Overall target:
  #     01 - 24 Preludes and Fugues, Op. 87: Prelude No. 1 in C Major.ogg
  # --> 01 - Shostakovich - Preludes and Fugues, Op 87 Nr 1, C major.ogg
  # Composer insertion:
  tarfspec = fspec  # start with a copy of the filepsec --
  # Reset the track#, if any (at start of filespec):
  tarfspec = tarfspec.gsub( /^(\d\d) /, "#{tracknr} - " )
  # Generic term fixes:
  tarfspec = tarfspec.gsub( 'for Piano', EMPTYSTR )
                     .gsub( /[ -][Mm]ajor/, ' major' )
                     .gsub( /[ -][Mm]inor/, ' minor' )
                     .gsub( /[Oo]p\./, 'Op' )
                     .gsub( /[Nn]o\./, 'Nr' )
                     .gsub( /[ -][Ss]harp/, '-sharp' )
                     .gsub( /[ -][Ff]lat/, '-flat' )
                     .gsub( ' in', ',' )
                     .gsub( ' .ogg', '.ogg' )
  # Fix general punctuation, remove problem-characters (Linux & Windows):
  tarfspec = tarfspec.gsub( ': ', ' - ' )
  tarfspec = tarfspec.gsub( '- -', '-' )
  tarfspec = tarfspec.gsub( '. ', ', ' )
  tarfspec = tarfspec.gsub( '?', EMPTYSTR )

  # Either rename or debug-echo:  $ musicfilerename [debug]
  if ARGV[0] == nil
    FileUtils.mv( fspec, tarfspec )
  else
    puts "\nmv: \"#{fspec}\""
    puts "--> \"#{tarfspec}\""
  end
  tracknr.succ!
end

exit true
