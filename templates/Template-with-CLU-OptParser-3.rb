#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# «·».rb
# from Template-with-CLU-OptParser-3.rb
#
# Copyright © 2022-2023 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v0.«·» («·»/«·»/2023)"
  AUTHOR = "Lorin Ricker, Valley Center, California, USA"

   CONFIGDIR = File.join( ENV['HOME'], ".config", PROGNAME )
  CONFIGFILE = File.join( CONFIGDIR, "#{PROGNAME}.yaml.rc" )

require 'optparse'    # standard library
require_relative 'lib/FileEnhancements'  # includes AppConfig class

include lib/CLU.rb

# ==========

# Add program/utility-specific Command Line Options here:
options = { :«+»     => «+»,
            :«+»     => «+»
          }

# Turn on/off Config-File option (default is off)
options.merge!( { :configfile => false } )  # change to true to enable...
# Add-in Common Options (--help, --about, --dryrun, --sudo,
#                        --verbose, --debug, --pry )
options.merge!( CLU.common_options )

# ==========

def config_save( opt )
  # opt is a local copy of options, so we can patch a few
  # values without disrupting the original/global hash --
  opt[:about]     = false
  opt[:debug]     = DBGLVL0
  opt[:«+»]       = «+»
  opt[:noop]      = false
  opt[:sudo]      = ""
  opt[:update]    = false
  opt[:verbose]   = false
  AppConfig.configuration_yaml( CONFIGFILE, opt, true )  # force the save/update
end if options[:configfile]

def oparse( options )
  optparse = OptionParser.new { |opts|
    opts.on( "-«+»", "--«+»",  # "=«+»", String,
             "«+»" ) do |val|
      options[:«+»] = «+»
    end  # -«+» --«+»
    opts.separator ""
    opts.separator "    The options below are always saved in the configuration file"
    opts.separator "    in their 'off' or 'default' state:"
    opts.on( "-S", "--sudo",
             "Run this backup/restore with sudo" ) do |val|
      options[:sudo] = "sudo"
    end  # -S --sudo
    opts.on( "-n", "--noop", "--dryrun", "--test",
             "Dry-run (test & display, no-op) mode" ) do |val|
      options[:noop]  = true
      options[:verbose] = true  # Dry-run implies verbose...
    end  # -n --noop
    opts.on( "-u", "--update", "--save",
             "Update (save) the configuration file; a configuration",
             "file is automatically created if it doesn't exist:",
             "#{CONFIGFILE}" ) do |val|
      options[:update] = true
    end  # -u --update
    # --- Verbose option ---
    opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
      options[:verbose] = true
    end  # -v --verbose
    # --- Debug option ---
    opts.on( "-D", "--debug", "=DebugLevel", Integer,
            "Show debug information (levels: 1, 2 or 3)",
            "  1 - enable basic debug information output",
            "  2 - enable advanced debug information output",
            "  3 - enable (starts) 'Ruby debug' gem debugger" ) do |val|
      options[:debug] = val.to_i
    end  # -D --debug
    opts.on( "-P", "--pry", "Debug with pry (rather than 'Ruby debug' gem)") do |val|
      options[:prydebug] = true
      options[:debug] = DBGLVL3
    end  # -P --pry
   # --- About option ---
    opts.on_tail( "-a", "--about", "Display program info" ) do |val|
      $stdout.puts "#{PROGID}"
      $stdout.puts "#{AUTHOR}"
      options[:about] = true
      exit true
    end  # -a --about
    # --- Set the banner & Help option ---
    opts.banner = "\n  Usage: #{PROGNAME} [options] «+»ARG«+»" +
                  "\n\n   where «+»\n\n"
    opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
      $stdout.puts opts
      # $stdout.puts "«+»Additional Text«+»"
      options[:help] = true
      exit true
    end  # -? --help
  }.parse!  # leave residue-args in ARGV
end # oparse

# Conditionally add Config-File option-values
options.merge!(
  AppConfig.configuration_yaml( CONFIGFILE, options ) ) if options[:configfile]

# Parse Command Line Options
oparse( options )

# === MAIN program ======

# f1 = ARGV[0] || ""  # a completely empty args will be nil here, ensure "" instead
# f2 = ARGV[1] || ""

# ...(optional) other command-line processing goes here...

# Update the config-file, at user's request:
config_save( options ) if options[:configfile]

# === Debugging starts here, if selected from Command Line...
CLU.debuggerRequired?( options[:debug] )

# === Application processing goes here...

exit true
