#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# «·».rb
#
# Copyright © 2020 Lorin Ricker <Lorin@RickerNet.us>
# Version 0.1, «·»/«·»/2020
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.

# PROGNAME = "dir"
#   PROGID = "#{PROGNAME} v0.1 («·»/«·»/2020)"
#   AUTHOR = "Lorin Ricker, Valley Center, California, USA"


# === Main ===
if $0 == __FILE__ then
  «·»
end  # Main
