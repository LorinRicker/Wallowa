#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# printat.rb
#
# Copyright © 2018 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

#

# Use:  $ printat
#       > 10,10 text to print
#       > ...
#       >        # Ctrl/D or empty-line to exit

PROGNAME = File.basename( $0, '.rb' )
  PROGID = "#{PROGNAME} v1.01 (09/14/2018)"
  AUTHOR = "Lorin Ricker, Elbert, Colorado, USA"

require_relative 'lib/TermChar'
require_relative 'lib/ANSIseq'
require_relative 'lib/GetPrompted'

def header( maxrow, maxcol )
  headstr = " #{PROGID.upcase} [#{maxrow}x#{maxcol}] "
  headlns = "=" * ( ( maxcol - headstr.length ) / 2 )
  return headlns + headstr + headlns
end  # header

def clearscreen( )
  String.cls
  maxrow, maxcol = TermChar.terminal_dimensions
  String.print_at( row: 1, col:1, text: header( maxrow, maxcol ), restorecursor: true )
  String.print_at( row: maxrow, col: 1, text: "", restorecursor: false )
  return [ maxrow, maxcol ]
end # clearscreen

# === Main ===
pstr = PROGNAME.downcase
maxrow, maxcol = TermChar.terminal_dimensions
String.cls
String.print_at( row: maxrow, col: 1, restorecursor: false )
String.print_at( row: 1, col:1, text: header( maxrow, maxcol ), restorecursor: true )

while intext = getprompted( pstr, "", false )  # 4,5 this is test text
  break if ( intext == "" || intext[0].downcase == "q" )
  maxrow, maxcol = clearscreen if intext == "cls"
  String.scrolldown( rows: 1 )
  String.print_at( row: 1, col:1, text: header( maxrow, maxcol ), restorecursor: true )
  String.eraseinline( code: 0 )
  pos = intext.split[0]
  row = pos.split(",")[0].to_i   # row,col ...
  col = pos.split(",")[1].to_i
  intext = intext.gsub( pos, "" ).strip
  String.print_at( row: row, col: col, text: intext, restorecursor: true )
  begin
    trap( 'WINCH' ) do
      clearscreen
      yield
    end
  rescue Interrupt => e
    ##raise SystemExit
    return nil
  end
end  # while

exit true
