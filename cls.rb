#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# cls.rb
#
# Copyright © 2018 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

# A quick-&-dirty auxiliary to the native bash 'clear' command

# Use:  $ cls        # clear screen, no change to background
#       $ cls dark   # clear screen, background dark/black
#       $ cls light  # clear screen, background light/white

PROGNAME = File.basename( $0, '.rb' )
  PROGID = "#{PROGNAME} v1.00 (09/13/2018)"
  AUTHOR = "Lorin Ricker, Elbert, Colorado, USA"

require_relative 'lib/ANSIseq'

background = ARGV[0] || "no-change"
String.cls( background )
exit true
