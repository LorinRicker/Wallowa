#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# TermChar.rb
#
# Copyright © 2012-2018 Lorin Ricker <Lorin@RickerNet.us>
# Version 3.3, 09/13/2018
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.

# Terminal Characteristics --

module TermChar

  def self.terminal_dimensions( show = false, os = false )
    # returns array as [ ROWS, COLUMNS ] or [ LENGTH, WIDTH ] or [ LINES, CHARS ]
    tdim = [ 24, 80 ]  # old VT-100 dimensions, reasonable fallback?
    if os
      whichos = os
    else
      require_relative '../lib/WhichOS'
      whichos = WhichOS.identify_os
    end
    # Determine window/term-screen size as [ LENGTH, WIDTH ]
    case whichos
    when :linux
      if RUBY_VERSION >= '2.0'  # use Ruby's own top-level constant
        require "io/console"
        tdim = IO.console.winsize  # [ ROWS, COLS ]
      else
        # Hack to get terminal's display dimensions [ ROWS, COLS ] --
        # stty size returns terminal's [heighth, width] (#rows, #columns):
        tdim = %x{stty size}.split.collect { |td| td.to_i }
      end
    when :vms
      tdim = [ %x{ WRITE sys$output F$GETDVI("TT","TT_PAGE") }.chomp.to_i,    # [ ROWS
               %x{ WRITE sys$output F$GETDVI("TT","DEVBUFSIZ") }.chomp.to_i ] # , COLS ]
    # when :unix
    # when :windows
    else
      puts "%TermChar-e-NYI, terminal_dimensions not yet implemented for \\#{whichos}\\"
    end  # case whichos
    if show
      puts "Terminal length is #{tdim[0]} rows (lines)"
      puts "Terminal width is #{tdim[1]} cols (chars)"
    end  # if show
    return tdim
  end  # terminal_dimensions

  def self.terminal_height  # rows, lines
    terminal_dimensions[0]
  end  # terminal_height

  def self.terminal_width  # cols, chars
    terminal_dimensions[1]
  end  # terminal_width

  def self.every_window_change_event
    begin
      puts ">>> ready to trap..."
      trap( 'WINCH' ) do
        yield
      end
      sleep
    rescue Interrupt => e
      raise SystemExit
      return nil
    end
  end  # each_window_change_event

end  # module TermChar
