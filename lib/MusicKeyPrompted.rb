#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# MusicKeyPrompted.rb
#
# Copyright © 2021 Lorin Ricker <Lorin@RickerNet.us>
# Version 1.0, 02/06/2021
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

require_relative 'TerminalPrompted'

def majorminor( chr )
  case chr
  when /[ABCDEFG]/
    return ' major'
  when /[abcdefg]/
    return ' minor'
  else
    STDERR.puts "%#{PROGNAME}-e-invalid_key, not a proper key name '#{chr}'"
  end  # case
end  # majorminor

def expandkey( keyabbr )
  case keyabbr.length
  when 1
    return keyabbr.upcase + majorminor( keyabbr )
  when 2
    case keyabbr[1]
    when '#' then accidental = '-sharp'
    when 'b' then accidental = '-flat'
    else
      STDERR.puts "%#{PROGNAME}-e-syntax, key-accidental entry error"
      return ''
    end  # case
    return keyabbr[0].upcase + accidental + majorminor( keyabbr[0] )
  else
    STDERR.puts "%#{PROGNAME}-e-syntax, key-name entry error"
    return ''
  end  # case
  return ''  # last-chance: should not occur...
end  # expandkey

def get_music_key( prompt )
  key = ''
  while key == ''
    keyabbr = getedited( prompt, keyabbr ).collapse
    if keyabbr == '?'
      STDERR.puts <<-HELPTEXT
          Enter key abbreviations as follows:

          'A', 'C' ...   for 'A major', 'C major', ...
          'b', 'd' ...   for 'B minor', 'D minor', ...
          'F#', 'G#' ... for 'F-sharp major', 'G-sharp major', ...
          'Bb', 'Eb' ... for 'B-flat major', 'E-flat major', ...
          'f#', 'g#' ... for 'F-sharp minor', 'G-sharp minor', ...
          'bb', 'eb' ... for 'B-flat minor', 'E-flat minor', ...

          HELPTEXT
      keyabbr = ''
    else
      key = expandkey( keyabbr )
    end
  end  # while
  return key
end  # get_music_key

alias getkey get_music_key
