#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# ANSIseq.rb
#
# Copyright © 2011-2018 Lorin Ricker <Lorin@RickerNet.us>
# Version 5.4, 10/10/2018
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.

# To render ANSI-escape-sequence effects on a compliant terminal(-emulator),
# such as a VT2xx, VT3xx, VT4xx, VT5xx-series or an X-term, use the following
# String methods:
#
#   str = 'string'.color(:red)
#   or...
#   puts "This is a test.".bold
#
# Or even in combinations, as long as the color attribute comes last
# (which is an ANSI-escape-sequence rendition limitation):
#
#   str = 'string'.bold.color(:red)
#   or...
#   puts "This is a test.".bold.underline.color(:purple)

class String

# match ANSI codes:
  ANSI_ESCSEQ_PATTERN = '(\[)?\033(\[)?[;?\d]*[\dA-Za-z]([\];])?'

  def sanitize_escseq( str )
    # Return str with any(all) ANSI escape-sequence(s) removed:
    str.gsub( /#{ANSI_ESCSEQ_PATTERN}/, '' )
  end  # sanitize_escseq

  def ansi_escseq?( str )
    # Check if str contains any ANSI escape-sequence (return true|false):
    !!( str =~ /#{ANSI_ESCSEQ_PATTERN}/ )
  end  # ansi_escseq?

  def only_ansi_escseq?( str )
    # Check if str contains only an ANSI escape-sequence (return true|false):
    !!( str =~ /^(#{ANSI_ESCSEQ_PATTERN})+$/ )
  end  # only_ansi_escseq?

# ANSI terminal rendition control-sequences:
  ESC = ?\033
 NORM = "\e[0m"  # normal rendition
 BOLD = "\e[1m"  # bold
 ULIN = "\e[4m"  # underline
 RVRS = "\e[7m"  # reverse
 BLNK = "\e[5m"  # blink (if supported by terminal or terminal emulator)
 CONC = "\e[8m"  # conceal, no-echo, invisible (ditto if supported...)

# Not widely (or ever) supported, here for reference/completeness:
#      FAINT = "\e[2m"  # faint (dim))
#     ITALIC = "\e[3m"  # italic
# BLINKRAPID = "\e[6m"  # "rapid"
#   CROSSOUT = "\e[9m"  # characters "marked out"

# Quick Ruby-one-liner tests:
#   $ ruby -e "require './lib/ANSIseq'; print String::CLRSCR"
#   $ ruby -e "require './lib/ANSIseq'; print String::CLRDARK"
#   $ ruby -e "require './lib/ANSIseq'; print String::CLRLIGHT"
#   $ ruby -e "require './lib/ANSIseq'; print String::CLRDARK,String::CLRSCR"
    CLRSCR = "\e[H\e[2J"      # ANSI terminal clear-screen
  CLRLIGHT = "\e[?5l"         #      set screen light-background
   CLRDARK = "\e[?5h"         #      set screen dark-background

      SAVECURSOR = "\e7"      # same as "\e[s", but more widely supported
   RESTORECURSOR = "\e8"      # same as "\e[u", ditto
# POSITIONCURSOR = "\e[r;cH"  # must be formed on-the-fly for row,col-values
    SCROLLSCR1UP = "\e[1S"    # form on-the-fly for
  SCROLLSCR1DOWN = "\e[1T"    # ...multi-lines up or down

# ANSI foreground or "normal intensity" colors (forces normal background)):
    BLACK = "\e[0;30m"    # normal black     [  0,  0,  0] \
      RED = "\e[0;31m"    # normal red       [187,  0,  0] |
    GREEN = "\e[0;32m"    # normal green     [  0,187,  0] | PuTTY
    BROWN = "\e[0;33m"    # normal yellow    [187,187,  0] | RGB
     BLUE = "\e[0;34m"    # normal blue      [  0,  0,187] | values
   PURPLE = "\e[0;35m"    # normal magenta   [187,  0,187] |
     CYAN = "\e[0;36m"    # normal cyan      [  0,187,187] |
   LTGRAY = "\e[0;37m"    # normal white     [187,187,187] /

# ANSI foreground or "bright intensity" colors (against reverse background)):
   DKGRAY = "\e[1;30m"    # bright black     [ 85, 85, 85] \
    LTRED = "\e[1;31m"    # bright red       [255, 85, 85] |
  LTGREEN = "\e[1;32m"    # bright green     [ 85,255, 85] | PuTTY
   YELLOW = "\e[1;33m"    # bright yellow    [255,255, 85] | RGB
   LTBLUE = "\e[1;34m"    # bright blue      [ 85, 85,255] | values
 LTPURPLE = "\e[1;35m"    # bright magenta   [255, 85,255] |
   LTCYAN = "\e[1;36m"    # bright cyan      [ 85,255,255] |
    WHITE = "\e[1;37m"    # bright white     [255,255,255] /

# Note:
#  For PuTTY terminal-emulator (VMS) --
#    Specify background color before foreground color --
#    WRITE sys$output BLUE_BG + WHITE + "This is a test!" + NORM
#  For xterm or Gnome Terminal (Linux) --
#    Combinations of background and foreground colors don't work,
#    background colors work best on dark/black background, and
#    text will appear as white characters here.
#    Background colors on white/light background are illegible
#    as to characters rendered within the background color.
#

# ANSI background colors:
      BLACK_BG = "\e[0;40m"    # normal black     [  0,  0,  0] \
        RED_BG = "\e[0;41m"    # normal red       [187,  0,  0] |
      GREEN_BG = "\e[0;42m"    # normal green     [  0,187,  0] | PuTTY
      BROWN_BG = "\e[0;43m"    # normal yellow    [187,187,  0] | RGB
       BLUE_BG = "\e[0;44m"    # normal blue      [  0,  0,187] | values
     PURPLE_BG = "\e[0;45m"    # normal magenta   [187,  0,187] |
       CYAN_BG = "\e[0;46m"    # normal cyan      [  0,187,187] |
  LIGHTGRAY_BG = "\e[0;47m"    # normal white     [187,187,187] /

def render( rendition )
  # Copy the object string (self) for testing, trim trailing whitespace,
  # test end-of-string characters so that only a single instance of NORM
  # is appended, then return the entire original string wrapped with
  # begin/end-rendition ANSI-escape-sequences:
  begin
    str  = self
    tstr = str.rstrip
    ln   = NORM.length
    str  = str + NORM unless tstr[-ln,ln] == NORM
    return rendition + str
  rescue
    return self
  end
end  # render

def bold
  self.render( BOLD )
end  # bold

def underline
  self.render( ULIN )
end  # underline

def reverse
  self.render( RVRS )
end  # reverse

# Render *any* of the available colors here;
# common colors each have their own method:
def color( colour )
  rendition = case colour.to_sym
              when :black    then BLACK
              when :red      then RED
              when :green    then GREEN
              when :brown    then BROWN
              when :blue     then BLUE
              when :purple   then PURPLE
              when :cyan     then CYAN
              when :ltgray   then LTGRAY
              when :dkgray   then DKGRAY
              when :ltred    then LTRED
              when :ltgreen  then LTGREEN
              when :yellow   then YELLOW
              when :ltblue   then LTBLUE
              when :ltpurple then LTPURPLE
              when :ltcyan   then LTCYAN
              when :white    then WHITE
              else
                $stderr.puts "%ANSIseq-w-nocolor, requested color not supported"
                BLACK
              end  # case colour
  self.render( rendition )
end  # color

def atpos( row: 1, col: 1, text: "", restorecursor: true )
  return String.atposition( row: row, col: col, text: text,
                            restorecursor: restorecursor )
end # atpos

# String class methods:

# Print/place self-text at a specific terminal window position by [row,col]
def self.atposition( row: 1, col: 1, text: "", restorecursor: true )
  require_relative './TermChar'
  trow, tcol = TermChar.terminal_dimensions
  row = [ row, trow ].min
  col = [ col, tcol ].min
  posstr = SAVECURSOR + "\e[#{row};#{col}H"
  $stdout.print ( $stdout.tty? ? posstr + text : text )
  $stdout.print RESTORECURSOR if restorecursor
  return posstr  # where text was actually printed...
end  # atposition

# Print/place self-text at a specific terminal window position by [row,col]
def self.print_at( row: 1, col: 1, text: "", restorecursor: true )
  return self.atposition( row: row, col: col, text: text,
                          restorecursor: restorecursor )
end # print_at

def self.scrollup( rows: 1 )  # pans the whole screen up by rows-lines
  $stdout.print ( $stdout.tty? ? "\e[#{rows}S" : "" )
  return rows
end # scrollup

def self.scrolldown( rows: 1 )  # pans the whole screen down by rows-lines
  $stdout.print ( $stdout.tty? ? "\e[#{rows}T" : "" )
  return rows
end # scrolldown

def self.eraseinline( code: 0 )  # erase in line
  # code : 0 - erase from cursor to end-of-line
  #        1 - erase from cursor to beginning-of-line
  #        2 - erase entire line
  code = 0 if ! (0..2).include?( code )
  $stdout.print ( $stdout.tty? ? "\e[#{code}K" : "" )
  return code
end # eraseinline

# String.clearscreen( 'dark' ) or String.clearscreen( 'black' ) or
# String.clearscreen( 'light' ) or String.clearscreen( 'white' ) or
# String.clearscreen  ...etc.
#   Returns the dark-clearscreen or light-clearscreen or just clearscreen
#   escape-sequence string if $stdout is a TTY, or an empty string otherwise.
def self.clearscreen( background = :clrscr )
  clrstr = case background.downcase.to_sym
  when :dark, :clsdark, :black
    CLRDARK + CLRSCR
  when :light, :clslight, :white
    CLRLIGHT + CLRSCR
  else  # :clrscr, or anything else...
    CLRSCR
  end  # case
  return ( $stdout.tty? ? clrstr : "" )
end  # clearscreen

# String.cls( 'dark' ) or String.cls( 'black' ) or
# String.cls( 'light' ) or String.cls( 'white' ) or
# String.cls
#   Fires the dark-clearscreen or light-clearscreen or just clearscreen
#   escape-sequence at $stdout if it is a TTY, or an empty string otherwise.
# Quick Ruby-one-liner tests:
#   $ ruby -e "require './lib/ANSIseq'; print String.cls('light')"
#   $ ruby -e "require './lib/ANSIseq'; print String.cls('dark')"
#   $ ruby -e "require './lib/ANSIseq'; print String.cls"
def self.cls( background = :clrscr )
  $stdout.print clearscreen( background )
end  # cls

end  # class String

# === Main/test/demo ===
#
# Test with:  $ ruby ./lib/ANSIseq.rb { "light" | "dark" | "" }
#
if $0 == __FILE__
  background = ARGV[0] || "no-change"
  String.cls( background )
  puts "\n#{'='*3} ANSI color demo #{'='*30}"
  colors = [ :black, :white,
             :red, :ltred, :blue, :ltblue, :green, :ltgreen,
             :purple, :ltpurple, :cyan, :ltcyan, :dkgray, :ltgray,
             :yellow, :brown ]
  x = "ABC.xyz!"
  colors.each do | c |
    cs = c.to_s
    ps = sprintf( "%10s: '%s' - '%s' - '%s'", cs, x.color(c),
                       x.bold.color(c), cs.underline.color(c) )
    puts ps
  end
  puts "#{'='*50}"
end
