#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# BoxIt.rb
#
# Copyright © 2020-2023 Lorin Ricker <Lorin@RickerNet.us>
# Version v.2.3 (07/01/2023)
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.

module BoxIt

  class LineGraphics < String
    include Enumerable

    # These "*Simple" tokens are the source-code "language" for drawing boxes --
    # horizontal lines are made of one or more '-' (HorizStrutSimple) chars;
    # vertical lines are made of one or more '|' (VertStrutSimple) chars;
    # any/all corners and intersections are made of a '+' (CrossSimple) char,
    # which then must be neighbor-tested in its context to determine exact
    # finished corner/tee/cross character to substitute.
    HorizStrutSimple = '-'
     VertStrutSimple = '|'
         CrossSimple = '+'
        StrutPattern = /[+|-]/
     StrutRunPattern = /[+| -]/  # Note: this includes a SPACE...

    NoStrutScore =  0
      NorthScore =  1
      EastScore  =  3
      SouthScore =  5
      WestScore  = 10

    # Because KED keymap diagrams require the use of characters '+' and '-'
    # to label individual keypad function keys, I've got to employ that old
    # wart-on-a-bag device, the escape (flag) character: '\' as usual. Thus,
    # a simple-box sequence like '\+' will generate a literal '+' character
    # into the finished_output_file, and '\-' will similarly produce a literal
    # '-' into the output (not leaving out '\|', but likely less-often needed).
    # As usual, use '\\' to generate a single literal '\' character, and using
    # the escape-flag on a non-token [^+|-] character is redundant, but produces
    # the non-token character literally as expected.
    #
    # Yes, because the use of an escape-char-sequence requires 2 characters to
    # represent a single literal character, the appearance of an esc-sequence
    # in a simple-box source line will right-shift the following text by one
    # character for each esc-sequence in that line.
    #
    # Interestingly, this marks the first time in my own coding experience where
    # I've employed both an escape flag ('\') *and* ANSI escape sequences (ESC)
    # in the same program...

    Literal       = '#'  # a placeholder for @@StrutMap fields...
    LiteralScore0 = 0
##    LiteralScore5 = 5

    # ANSI characters for finished boxes, using DEC Special Graphics char-set --
    # escape sequence "<esc>(0" must introduce/switch-in the VT100 Graphics
    # character set (G1):
    HorizStrutANSI = "\x71"
     VertStrutANSI = "\x78"
      NECornerANSI = "\x6B"
      SECornerANSI = "\x6A"
      SWCornerANSI = "\x6D"
      NWCornerANSI = "\x6C"
      NorthTeeANSI = "\x77"
       EastTeeANSI = "\x75"
      SouthTeeANSI = "\x76"
       WestTeeANSI = "\x74"
         CrossANSI = "\x6E"

    ESC = "\x1B"  # Escape character
    G1  = ESC + ')0'
    SO  = "\x0E"  # Shift-Out, switch into alternate character set (graphics)
    SI  = "\x0F"  # Shift-In, back to default/regular character set
                  # In-editor graphics: '' = SI, '' = SO
    SOVISIBLE = "\x7B"  # "{"
    SIVISIBLE = "\x7D"  # "}"

    # UTF8 (Unicode) characters for finished boxes:
    HorizStrutUTF8 = "\u2500"
     VertStrutUTF8 = "\u2502"
      NECornerUTF8 = "\u2510"
      SECornerUTF8 = "\u2518"
      SWCornerUTF8 = "\u2514"
      NWCornerUTF8 = "\u250C"
      NorthTeeUTF8 = "\u252C"
       EastTeeUTF8 = "\u2524"
      SouthTeeUTF8 = "\u2534"
       WestTeeUTF8 = "\u251C"
         CrossUTF8 = "\u253C"

  StrutMapHorizPos = 1  # these two positions are assigned for convenience...
  StrutMapVertPos  = 2

    def initialize( simpletext, options )
    # Strut-Cross Mapping (translation) table:
    #                                 14 (SouthTee)
    #                                    (N)
    #                                     1
    #                11 (SECorner)                 4 (SWCorner)
    #    ^                                |
    #    |                                |
    #  -(N)-   16 (EastTee) (W) 10  ------+------  3 E (9) (WestTee)
    #    |                                |
    #    |                                |
    #                15 (NECorner)                 8 (NWCorner)
    #                                     5
    #                                    (S)       19 (Cross)
    #                                 18 (NorthTee)
    #
    # To determine which corner/tee/cross character to substitute for a '+'...
    # 1) Label the four-legged cross above with four numbers, one for each leg.
    # 2) The four numbers are chosen such that each and every two-legged, or
    #    three-legged, or the single four-legged values, when summed, gives a
    #    *unique* total/sum value, which in turn selects the correct finished
    #    corner, tee or cross character.
    # 3) For example, the north leg's 1 plus the east leg's 3 sums to 4,
    #    which is assigned to (gives) a SW corner finished character.
    #    The west leg's 3, plus the south leg's 5, plus the east leg's 10
    #    sum to 18 for a South tee finished character.
    #    All four legs, 1 + 3 + 5 + 10 sum to 19, for the finished Cross
    #    character.
    #
    # This array of hashes allows simple-to-finished character lookups,
    # with the array index determining the finished corner, tee or cross
    # once the simple "+"'s neighborhood score is summed; however, the
    # values for Horizontal and Vertical Struts, 0 & 1, are assigned, not
    # summed; and any nil value "cannot happen" (invalid sum), thus error.
    #
    # --------------
    # Small epiphany:  I was originally planning to treat the simpletext
    # array as a "flat sheet of paper with edges," that is, when evalu-
    # ating corners '+', there is "nothing" to the north of row 0 (above
    # it), and "nothing" to the west (left) of position 0... similarly
    # for the east (right) and south (bottom).  Seems reasonable...
    # However, the array index notation Ary[-1] has a perfectly natural
    # and practical meaning -- it's the "last value" of the array.  This
    # means that, by *allowing* evaluation "behind" or "above" or "beyond"
    # the natural "page edge" boundaries, we can treat the "page" as a
    # two-edge-connected compound cylinder. and thus evaluate a corner '+'
    # as influenced by what's on the opposite, far edge!  Is this useful?
    # Or should the "flat page with edges" model prevail?
    # I'm still thinking about this -- experiments are in order...

    @@StrutMap = Array.new
    @@StrutMap = [                                   # array index:
      { simple: Literal,                             #  0
        ansi: Literal, utf8: Literal,
        descr: "A literal '+' or '-' in a label or legend" },
      { simple: HorizStrutSimple,                    #  1 (StrutMapHorizPos)
        ansi: HorizStrutANSI, utf8: HorizStrutUTF8,
        descr: "Horizontal Strut" },
      { simple: VertStrutSimple,                     #  2 (StrutMapVertPos)
        ansi: VertStrutANSI, utf8: VertStrutUTF8,
        descr: "Vertical Strut" },
      nil,                                           #  3
      { simple: CrossSimple,                         #  4
        ansi: SWCornerANSI, utf8: SWCornerUTF8,
        descr: "Southwest Corner" },
      nil, #{ simple: Literal,                       #  5
        #ansi: Literal, utf8: Literal,
        #descr: "«» A literal [+|-]" },
      nil,                                           #  6
      nil,                                           #  7
      { simple: CrossSimple,                         #  8
        ansi: NWCornerANSI, utf8: NWCornerUTF8,
        descr: "Northwest Corner" },
      { simple: CrossSimple,                         #  9
        ansi: WestTeeANSI, utf8: WestTeeUTF8,
        descr: "West Tee" },
      nil,                                           # 10
      { simple: CrossSimple,                         # 11
        ansi: SECornerANSI, utf8: SECornerUTF8,
        descr: "Southeast Corner" },
      nil,                                           # 12
      nil,                                           # 13
      { simple: CrossSimple,                         # 14
        ansi: SouthTeeANSI, utf8: SouthTeeUTF8,
        descr: "South Tee" },
      { simple: CrossSimple,                         # 15
        ansi: NECornerANSI, utf8: NECornerUTF8,
        descr: "Northeast Corner" },
      { simple: CrossSimple,                         # 16
        ansi: EastTeeANSI, utf8: EastTeeUTF8,
        descr: "East Tee" },
      nil,                                           # 17
      { simple: CrossSimple,                         # 18
        ansi: NorthTeeANSI, utf8: NorthTeeUTF8,
        descr: "North Tee" },
      { simple: CrossSimple,                         # 19
        ansi: CrossANSI, utf8: CrossUTF8,
        descr: "Center Cross" }
      ]
      @options = options
      @simpletext = simpletext
      @finishlines = Array.new( @simpletext.size, "" )
      pp @@StrutMap if @options[:debug] >= DBGLVL3
      pp @finishlines if @options[:debug] >= DBGLVL3
    end  # initialize

    def print( outputfile )
      File.open( outputfile, 'w' ) do | fout |
        @finishlines.each { | line | fout.puts line }
      end
    rescue Errno::ENOENT => e
      STDERR.puts "%#{PROGNAME}-e-fnf, error opening output file (no such dir/path)"
      exit false
    rescue IOError => e
      STDERR.puts "%#{PROGNAME}-e-errout, error opening output file or stream"
      exit false
    end  # self.print

    def north_score( chkline, curpos )
      north = case @options[:page]
              when :wrap
                chkline
              when :flat
                chkline >= 0 ? chkline : false
              end  # case
      # north = @options[:page] == :wrap ? chkline : false
      # return ( @simpletext[north][curpos] == CrossSimple ?
      #   NorthScore : NoStrutScore ) if north
      return ( @simpletext[north][curpos] == VertStrutSimple ?
                  NorthScore : NoStrutScore ) if north
      return 0  # if flat-page on edge
    end  # north_score

    def east_score( curline, chkpos )
      chkpos = 0 if chkpos >= @simpletext[0].length  # all lines padded...
      east = case @options[:page]
              when :wrap
                chkpos
              when :flat
                (0...@simpletext[0].length).include?(chkpos) ? chkpos : false
              end  # case
      # east = @options[:page] == :wrap ? chkpos : false
      return ( @simpletext[curline][east] == HorizStrutSimple ?
                  EastScore : NoStrutScore ) if east
      return 0  # if flat-page on edge
    end  # east_score

    def south_score( chkline, curpos )
      chkline = 0 if chkline >= @simpletext.length
      south = case @options[:page]
              when :wrap
                chkline
              when :flat
                chkline >= 0 ? chkline : false
              end  # case
      # south = @options[:page] == :wrap ? chkline : false
      return ( @simpletext[south][curpos] == VertStrutSimple ?
                  SouthScore : NoStrutScore ) if south
      return 0  # if flat-page on edge
    end  # south_score

    def west_score( curline, chkpos )
      west = case @options[:page]
              when :wrap
                chkpos
              when :flat
                (0...@simpletext[0].length).include?(chkpos) ? chkpos : false
              end  # case
      # west = @options[:page] == :wrap ? chkpos : false
      return ( @simpletext[curline][west] == HorizStrutSimple ?
                  WestScore : NoStrutScore ) if west
      return 0  # if flat-page on edge
    end  # west_score

    def to_linedrawn
      so = si = ""   # assume UTF8, so no shift-out/in
      if ( @options[:charset] == :ansi )
        so, si = ! @options[:visible] ? [SO, SI] : [SOVISIBLE, SIVISIBLE]
        @finishlines[0] = G1 + so  # select the ANSI line-drawing char-set
      end
      cset = @options[:charset]
      shiftstate = false  #  true when in/on the line-draw itself;
                          #       when SO-shifted in...
                          # false when in the "normal" character region;
                          #       when SI-shifted out...
      @simpletext.each_with_index do | line, curline |
        # STDOUT.puts "=== line #{curline}: '#{line}'" if @options[:debug] >= DBGLVL3
        # For each character on this line, either copy it verbatim to
        # the finished-lines, or determine it's line-drawing substitute:
        line.each_char.with_index do | chr, curpos |
          # Well I don't know why I came here tonight,
          # I got the feeling that something ain't right,
          # I'm so scared in case I fall off my chair,
          # And I'm wondering how I'll get down the stairs,
          # Clowns to the left of me,
          # Jokers to the right, here I am,
          # Stuck in the middle with you...
          #               -- Gerry Rafferty (Stealers Wheel, 1972)
          #
          if ( !shiftstate )
            # Calculate the corner/cross's "score" (neighborhood);
            # this selects the correct corner for the char-set:
            score = north_score( curline - 1, curpos ) \
                  +  east_score( curline, curpos + 1 ) \
                  + south_score( curline + 1, curpos ) \
                  +  west_score( curline, curpos - 1 )
          else  # Still in a linedrawing region, look at next character to continue?
            if ( chr =~ StrutRunPattern )
              # A match here is result 0 (character matched a candidate at pos-0), else nil:
              # It's okay to span/include SPACEs in this linedrawing run...
              # XXX:
            else
              # XXX:
            end
          end  # !shiftstate
          if ( score == LiteralScore0 ) # || ( score == LiteralScore5 )
            # a literal character, even if chr is [+|-] used as a legend...
            @finishlines[curline] += so if shiftstate
            @finishlines[curline] += chr
            shiftstate = false if shiftstate
            next
          end
          case chr
          when HorizStrutSimple
            @finishlines[curline] += si if !shiftstate
            # Line [1] of @@StrutMap holds the Horizontal Strut char:
            @finishlines[curline] += @@StrutMap[StrutMapHorizPos][cset]
            shiftstate = true if ! shiftstate
          when VertStrutSimple
            @finishlines[curline] += si if !shiftstate
            # Line [2] of @@StrutMap holds the Vertical Strut char:
            @finishlines[curline] += @@StrutMap[StrutMapVertPos][cset]
            shiftstate = true if ! shiftstate
          when CrossSimple
            pp ">> score = #{score}"
            if @@StrutMap[score]  # bad values are tagged as nil...
              @finishlines[curline] += si if !shiftstate
              @finishlines[curline] += @@StrutMap[score][cset]
              shiftstate = true if ! shiftstate
            else
              # A bad cross position is:
              #         '+----+----+----+' the 2nd '+' from left;
              #         '|         |    |'
              #     or: '+         |    |' which has no horiz-line
              #         '|         +    |'   leading or following.
              #         '|         |    +'
              #         '|         |    |'
              # Neighbor-score evaluation of these yields score values which
              # have no corresponding corner character, so these are simply
              # left in the finished output file as '+' characters for the
              # user to find and replace in the simple_box_input file.
              @finishlines[curline] += CrossSimple
              if @options[:debug] >= DBGLVL1
                msg = "%#{PROGNAME}-w-badcrosspos, "
                msgind = ' ' * msg.length
                STDERR.puts "#{msg}a simple cross mis-positioned"
                STDERR.puts "#{msgind}at SimpleText line #{curline}, pos #{curpos}"
              end
            end
          else
            # Just an ordinary character, but must determine
            # the transition from SO-state to SI again...
            @finishlines[curline] += so if shiftstate
            @finishlines[curline] += chr
            shiftstate = false if shiftstate
          end  # case chr
        end
        # Finish off each current line:
        @finishlines[curline] += so  #
        puts( "'#{@finishlines[curline]}'" ) if @options[:debug] >= DBGLVL3
      end
      cleanupKludge( si, so, options )
    end  # self.to_linedrawn

  # For reasons yet unknown (resists analysis and debugging), one specific form
  # of box corners is not being detected and finished, so the following just
  # kludges it in with brute-force:
  def cleanupKludge( si, so, options )
    @finishlines.each do | line |
      ansi_fixed, utf8_fixed = false
      case options[:charset]
      when :ansi
        line.gsub( '-j', 'qj' )  # in-editor graphics: '' = SI, '' = SO
        line.gsub( 'm-', 'mq' )
        ansi_fixed = true
      when :utf8
        line.gsub( '-┘', '─┘' )
        line.gsub( '└-', '└─' )
        utf8_fixed = true
      end  # case options[:charset]
    end  # @finishlines.each
    puts "%kludge-fixed, ANSI corners"  if ansi_fixed
    puts "%kludge-fixed, UTF-8 corners" if utf8_fixed
  end  # cleanupKludge

  end  # class LineGraphics

end  # module BoxIt
