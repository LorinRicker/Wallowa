#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# CountLines.rb
#
# Copyright © 2020 Lorin Ricker <Lorin@RickerNet.us>
# Version 0.1, 12/22/2020
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

# Simply counts lines in a file.

class File

def File.count_lines( srcfile )
  lcount = 0
  maxlen = 0
  srcfile = File.expand_path( srcfile ) if srcfile[0] != SEPARATOR
  File.open( srcfile, mode: 'r' ) do | f |
    while line = f.gets
      llen = line.chomp.length
      lcount += 1
      maxlen  = llen if maxlen < llen
    end  # while
    return [ lcount, maxlen ]
  end  # File.open
rescue Errno::ENOENT => e
  STDERR.puts "%CountLines-e-fnf, error opening input file '#{srcfile}'"
  exit false
end  # count_lines

end  # class File
