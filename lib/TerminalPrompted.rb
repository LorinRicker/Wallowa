#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# TerminalPrompted.rb
#   combines old/obsolete AskPrompted.rb, GetPrompted.rb, Prompted.rb and
#   SimplePrompted.rb (all of which may be fully deleted from this repository
#   at some future date...)
#
# Copyright © 2011-2021 Lorin Ricker <Lorin@RickerNet.us>
# Version 6.1, 02/10/2021
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

require_relative 'StringIsIn'
# require and include done here as a courtesy, as nearly all uses
# of prompting can benefit from readline completions:
require_relative 'AppCmdCompletions'
include AppCmdCompletions

# iniReadline sets (or clears) Readline's pre_input_hook, which "seeds" the
# readline text-input buffer with preliminary (default value), editable text;
# thus, the user can use standard readline editing command keystrokes to directly
# change/alter/update or completely replace that default-value text.
# Parameter value: Any non-empty text string will be set as default text.
#                  Pass an empty string "" or nil to clear the hook.
# This method can be called as often as necessary to set or clear default text
# for "the next readline"...
# This method (capability) was suggested by this StackOverflow article:
# https://stackoverflow.com/questions/2314105/what-will-give-me-something-like-ruby-readline-with-a-default-value
def initReadline( initext = nil )
  text = initext.to_s
  if text == ''
    Readline.pre_input_hook = nil
  else
    Readline.pre_input_hook = -> do
      Readline.insert_text( text )
      Readline.redisplay
    end
  end
end  # initReadline

# getedited returns a +response+ (string) from the user as prompted,
# or returns a +default+ response (string) if available and the user
# accepts that default by pressing the <Enter> key.
# Prompt-termination punctuation is a colon ":".
# returnexit set true lets caller receive and handle the exit itself.
def getedited( prompt, default = '', returnQuit = false )
  initReadline( default )
  response = Readline.readline( prompt, true ).strip  # 'true' -> add line to history
  if response.isIn?( %w{ exit quit } )
    # Always restore terminal echo:
    `stty echo`  # TODO: what about VMS?
    exit true  # this exit always provides cmd-line status:0
  end
  return response   # always return what the user edited/entered...
rescue StandardError => e   # this handles Ctrl/D, Ctrl/C, etc.
  return 'quit' if returnQuit     # to let caller handle the exit on its own...
  # otherwise:
  # Always restore terminal echo:
  `stty echo`  # TODO: what about VMS?
  exit true  # this exit always provides cmd-line status:0
end #getedited

# getprompted returns a +response+ (string) from the user as prompted,
# or returns a +default+ response (string) if available and the user
# accepts that default by pressing the <Enter> key.
# Prompt-termination punctuation is a colon ":".
# returnexit set true lets caller receive and handle the exit itself.
def getprompted( prompt, default = '', returnQuit = false )
  pstr = prompt + ( default == '' ? ": " : " [#{default}]: " )
  response = Readline.readline( pstr, true ).strip  # 'true' -> add line to history
  if response.isIn?( %w{ exit quit } )
    # Always restore terminal echo:
    `stty echo`  # TODO: what about VMS?
    exit true  # this exit always provides cmd-line status:0
  end
  return ( response != "" ? response : default )
rescue StandardError => e   # this handles Ctrl/D, Ctrl/C, etc.
  return 'quit' if returnQuit     # to let caller handle the exit on its own...
  # otherwise:
  # Always restore terminal echo:
  `stty echo`  # TODO: what about VMS?
  exit true  # this exit always provides cmd-line status:0
end #getprompted

def getprompted_noecho( prompt, default, returnQuit = false )
  require_relative 'WhichOS'
  os  = WhichOS.identify_os
  case os
  when :linux
    echooff = "stty -echo"
    echoon  = "stty echo"
  when :vms
    echooff = "SET TERMINAL /NOECHO"
    echoon  = "SET TERMINAL /ECHO"
  end  # case os
  `#{echooff}`
  response = getprompted( prompt, default, returnQuit )
  `#{echoon}`
  return response
end # getprompted_noecho

# askprompted returns a +true+ (boolean) if the user enters an affirmative
# '[Yy]...' response to the prompt, or returns a +false+ response (boolean)
# if the user enters a negative '[Nn]...' response (or any non-affirmative
# response); the +default+ response can be set to either "Y" or "N" ("Y" is
# the default +default+).
# Prompt-termination punctuation is a question mark "?".
# For example:
#   if askprompted( "Continue" ) then ...
#   if askprompted( "Continue", "N" ) then...
def askprompted( prompt, default = "Y" )
  # expects a Yes or No response,
  # returns true for any response beginning with "Y" or "y",
  # returns false for everything else...
  # but does test & respond to exit/quit/Ctrl-D/Ctrl-Z...
  dstr ||= default
  pstr = prompt + ( dstr == "" ? " (y/n)? " : " (y/n) [#{dstr}]? " )
  answer = Readline.readline( pstr, true ).strip  # 'true' -> add line to history
  if response.isIn?( %w{ exit quit } )
    # Always restore terminal echo:
    `stty echo`
    exit true  # this exit always provides cmd-line status:0
  end
  answer = dstr if answer == ""
  return ( answer[0].downcase == "y" ? true : false )
rescue StandardError => e
  `stty echo`
  exit true  # this exit always provides cmd-line status:0
end #askprompted

# SimplePrompted returns a +true+ (boolean) if the user enters an affirmative
# '[Yy]...' response to the prompt, or returns a +false+ response (boolean)
# if the user enters a negative '[Nn]...' response (or any non-affirmative
# response); the +default+ response can be set to either "Y" or "N" ("Y" is
# the default +default+).
# Prompt-termination punctuation is a question mark "?".
# This variant does not use Standard Library Readline/readline.
# For example:
#   if simpleprompted( "Continue" ) then ...
#   if simpleprompted( "Continue", "N" ) then...
def simpleprompted( prompt, default = "Y" )
  # expects a Yes or No response,
  # returns true for any response beginning with "Y" or "y",
  # returns false for everything else...
  # but does test & respond to exit/quit/Ctrl-D/Ctrl-Z...
  dstr ||= default
  pstr = prompt + ( dstr == "" ? " (y/n)? " : " (y/n) [#{dstr}]? " )
  STDOUT.print( pstr )
  answer = STDIN.readline.strip
  exit true if answer.downcase == "exit" || answer.downcase == "quit"
  answer = dstr if answer == ""
  return ( answer[0].downcase == "y" ? true : false )
rescue StandardError
  exit true  # this exit always provides cmd-line status:0
end #simpleprompted

def promptcycle( prompt, default, pat, promptions )
  goodval = nil
  while ! goodval
    val = getedited( prompt, default )
    within  = ""
    goodval = val.match( pat )
    #~ puts "goodval: '#{goodval ? "true" : "nil" }'"
    if goodval
      #~ response = val.???conversion???
      if promptions[:validrange]
        within  = " within " + promptions[:validrange].to_s
        goodval = promptions[:validrange].cover?( response )
      end
    end
    $stdout.puts "\n#{promptions[:reprompt]}#{within}\n" if ! goodval
  end  # while !goodval
  return [ goodval, goodval ? val : nil ]
end  # promptcycle

# Parameter options +promptions+ (hash) has the following (optional) keys:
#   :reprompt (string)   -- A re-prompt displayed if user enters
#                           an incorrect (bad) value for the type
#                           of data requested
#   :validrange (range)  -- A range (min..max) which covers the
#                           valid values for the user to enter
def ultraprompted( prompt, default = "Y", promptions = {} )
  default ||= default
  resptype  = default.class.to_s
  phrase    = resptype.article( true )
  # Default reprompt string, caller should provide a better/specific one:
  promptions[:reprompt] ||= "Bad value, please re-enter #{phrase}"
  case resptype
  when "String"
    response = getedited( prompt, default )
  when "Integer", "Fixnum", "Bignum"
    pat = /^\w*\d+\w*$/
    goodval, val = promptcycle( prompt, default, pat, promptions )
    response = goodval ? val.to_i : nil
  when "Float"
    pat = /^\w*\d+\.?\d*\w*$/
    goodval, val = promptcycle( prompt, default, pat, promptions )
    response = goodval ? val.to_f : nil
  else
    response = nil
    $stderr.puts "%ultraprompted-e-unktyp, unknown type #{resptype}"
  end  # case dclass
  return response
#~ rescue StandardError
  #~ exit true  # this exit always provides cmd-line status:0
end  # ultraprompted
