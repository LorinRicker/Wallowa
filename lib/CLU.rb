#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# lib/CLD.rb  -- Command Line Debugger/Definitions
#
# Copyright © 2022-2023 Lorin Ricker <Lorin@RickerNet.us>
# Version info: v0.04, 04/09/2023
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.

# Command Line Debugger/Definitions:  A module and namespace to support
# a DRY(er) version of establishing a Ruby debugger (gems debug or pry)
# ...

module CLU

#######################################################################
# Debug-Levels, activate various debug modes                          #
#  these values are assigned to and compared by options[:debug]       #
#######################################################################
DBGLVL0 = 0  # light, debugging-with-print: pp ... if options[:debug] #
DBGLVL1 = 1  # in-depth show-your-work, as in:                        #
DBGLVL2 = 2  #    puts ... if options[:debug] >= DBGLVL[1,2]          #
DBGLVL3 = 3  # binding.pry &/or pry-{byebug|nav}                      #
#######################################################################

  # ==========

  def common_options
  return { :help     => false,
           :about    => false,
           :dryrun   => false,
           :sudo     => "",
           :verbose  => false,
           :debug    => DBGLVL0,
           :prydebug => false
         }
  end  # common_options

  def debuggerRequired?( debugflag = false, pryflag = false )
    ###################################
    # Revised version of this block   #
    # now permits use of either       #
    # Ruby Debug gem or Pry gem       #
    ###################################
    if debugflag >= DBGLVL3           #
      if pryflag            # -P      #
        require 'pry'                 #
        binding.pry                   #
      else                            #
        require 'debug/start' # -Dn   #
        binding.break                 #
      end                             #
    end                               #
    ###################################
  end  # debuggerRequired?

  # def debugging?( )
  #   xxx
  # end  # debugging?

end  # CLU
