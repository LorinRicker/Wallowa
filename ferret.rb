#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# ferret.rb
#
# Copyright © 2019 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

# Wrapper/sanity for the find utility, which itself has a nearly
# impenetrable and complex command line syntax wrt switches, etc.
#

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v1.4 (11/27/2019)"
  AUTHOR = "Lorin Ricker, Valley Center, California, USA"

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for binding.pry &/or pry-{byebug|nav} #
             ######################################################

COMMA = ','.freeze

require 'optparse'
require 'open3'

# ============

def disallow( option )
  STDERR.puts "%#{PROGNAME}-e-illegal, use only one of: --root --home --user --path; combinations disallowed"
  STDERR.puts "  illegal-combo option: --#{option}"
  exit false
end  # disallow

def build_excludestr( options )
  exclusions = ""
  excludir = Array.new
  # For '/', exclude these specials and mount-points:
  excludir = %w{ /media /mnt /proc /run } if options[:root]
  if options[:exclusions]
    # Do NOT expand paths with ::absolute_path(), find won't use it correctly:
    options[:exclusions].split( COMMA ) { | e | excludir << e.strip }
    STDOUT.puts "excludir: '#{excludir}'" if options[:debug] >= DBGLVL2
  end
  excludir.each { | e | exclusions += "-path '#{e}' -prune -o " }
  STDOUT.puts "exclusions: '#{exclusions}'" if options[:debug] >= DBGLVL1
  exclusions
end  # build_excludestr

def build_filetree( args, start_at, options )
  filetree = Hash.new
  excl = build_excludestr( options )
  sudo = options[:sudo] ? "sudo " : ""
  type = "-type f,d"
  name = options[:exact] ? "-name" : "-iname"
  args.each do | arg |
    findcmd  = "#{sudo}find #{start_at} #{excl}#{type} #{name} '#{arg}' -print"
    STDERR.puts "%#{PROGNAME}-i-echo, cmd:\n#{findcmd}" if options[:verbose] || options[:commandonly]
    # 3-way plumbing...  Note that find can still throw certain kinds of errors,
    # even with sudo privs, e.g.: find: ‘/run/user/1000/doc’: Permission denied
    # as /run/user... is a special file (not regular), a "don't care" case.
    Open3.popen3( findcmd ) do | stdin, stdout, stderr, thrd |
      stdout.each do | ln |
        ln = ln.chomp
        thispath = File.absolute_path( File.dirname( ln ) )
        thisfile = File.basename( ln )
        # Create an (empty) array of files if this hash-(sub)directory doesn't yet exist:
        filetree[ thispath ] = [] if !filetree.key?( thispath )
        # Collect files by (sub)directory:
        filetree[ thispath ] << thisfile
      end
      if thrd.value.exitstatus > 1  # Process::Status
        STDERR.puts "%#{PROGNAME}-e-exitstatus, find subproc exit status: #{thrd.value.exitstatus}"
        exit thrd.value.exitstatus
      end
    end unless options[:commandonly]
  end
  filetree
end  # build_filetree

def report( filetree, options )
  fcount = 0
  filetree.sort.each do | subdir |  # ...the hash becomes an (ordered) array-of-arrays
    case options[:reporttype]
    when :vms                       # like a VMS DIR format
      STDOUT.puts "\n#{subdir[0]}\n"
      subdir[1].sort { |a,b| a.downcase <=> b.downcase } # case-insensitive sort
        .each { | fspec |
        STDOUT.puts "  #{fspec}"
        fcount += 1
      }
    when :linux                     # traditional, like find's output
      subdir[1].sort { |a,b| a.downcase <=> b.downcase } # case-insensitive sort
        .each { | fspec |
        STDOUT.puts "#{File.join( subdir[0], fspec )}"
        fcount += 1
      }
    end  # case
  end
  if fcount == 0 && !options[:commandonly]
    STDOUT.puts "%#{PROGNAME}-i-nofiles, no matching files found"
  else
    if options[:reportcount]  # tag on the file-count footer, if requested
      case options[:reporttype]
      when :vms
        msg = sprintf( "%d file%s found", fcount, fcount == 1 ? "" : "s" )
        STDOUT.puts "\n%#{PROGNAME}-i-count, #{msg}"
        STDOUT.puts "\n"
      when :linux
        STDOUT.puts "#{fcount}"
      end
    end
  end
end  # report

# === Main ===
options = { :user        => false,
            :start       => false,
            :root        => false,
            :home        => false,
            :sudo        => false,
            :exact       => false,
            :reporttype  => :vms,
            :reportcount => false,
            :exclusions  => false,
            :commandonly => false,
            :verbose     => false,
            :debug       => DBGLVL0,
            :about       => false
          }
start_at = "."  # initial assumption: "start here..."

optparse = OptionParser.new { |opts|
  opts.on( "-u", "-~", "--user",
           "File search starts at your (user's) $HOME (~)",
           "directory, searches recursively through your",
           "entire home-directory-tree" ) do |val|
    disallow( :user ) if options[:home] || options[:root] || options[:start]
    options[:user] = true
    start_at = "~"
  end  # -u --user
  opts.on( "-p", "--dir", "--path=DIR", String,
           "File search starts at this (sub)directory,",
           "searches recursively through entire sub-tree;",
           "enter as a relative '../there' or absolute",
           "'usr/lib' path in the file system" ) do |val|
    disallow( :start ) if options[:home] || options[:root] || options[:user]
    options[:start] = true
    start_at = File.absolute_path( val )
  end  # -p --path
  opts.on( "-H", "--home",
           "File search starts at top of /home directory,",
           "searches recursively through all user directory",
           "trees; uses sudo for this operation" ) do |val|
    disallow( :home ) if options[:user] || options[:root] || options[:start]
    options[:home] = options[:sudo] = true
    start_at = "/home"
  end  # -H --home
  opts.on( "-R", "--root",
           "File search starts at system's root '/' (top)",
           "directory level, searches recursively through",
           "the entire file-system hierarchy; uses sudo for",
           "this operation; auto-excludes /run, /mnt and",
           "/media subdirectories (trees)" ) do |val|
    disallow( :root ) if options[:home] || options[:user] || options[:start]
    options[:root] = options[:sudo] = true
    start_at = "/"
  end  # -R --root
  opts.on( "-S", "--sudo",
           "Use sudo for this file search (you'll need your",
           "password); sudo is auto/implied for the --root",
           "and --home options" ) do |val|
    options[:sudo] = true
  end  # -S --sudo
  opts.on( "-e", "--exact",
           "Match filespec's Up/low-Case naming exactly;",
           "default is case-insensitive" ) do |val|
    options[:exact] = true
  end  # -e --exact
  opts.on( "-r", "--report", "--style=REPORT", String, /vms|linux/i,
           "Type of report:  VMS (default) for block-",
           "style (like DIR), or LINUX for traditional",
           "path/basename style (one line per path/file)" ) do |val|
    options[:reporttype] = val.to_sym
  end  # -r --style
  opts.on( "-c", "--count",
           "Report count of files found" ) do |val|
    options[:reportcount] = true
  end  # -c --count
  opts.on( "-x", "--exclude=LIST", String,
           "Exclude these dir-trees (paths) from search,",
           "entered as a comma-separated list:",
           "'./.git,../path1' etc." ) do |val|
    options[:exclusions] = val
  end  # -x --exclude
  opts.on( "-!", "--command", "--only",
           "Just echo/report the 'find' command(s) generated" ) do |val|
    options[:commandonly] = true
  end  # -! --command
  opts.separator ""
  # --- Verbose option ---
  opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
    options[:verbose] = true
  end  # -v --verbose
  # --- Debug option ---
  opts.on( "-d", "--debug", "=DebugLevel", Integer,
           "Show debug information (levels: 1, 2 or 3)",
           "  1 - enables basic debugging information",
           "  2 - enables advanced debugging information",
           "  3 - enables (starts) pry-byebug debugger" ) do |val|
    options[:debug] = val.to_i
  end  # -d --debug
  # --- About option ---
  opts.on_tail( "-a", "--about", "Display program info" ) do |val|
    $stdout.puts "#{PROGID}"
    $stdout.puts "#{AUTHOR}"
    options[:about] = true
    exit true
  end  # -a --about
  # --- Set the banner & Help option ---
  opts.banner = "\n  Usage: #{PROGNAME} [options] filespec... [ >outfile ]" +
                "\n\n    where filespec... is the name of one or more text files," +
                "\n    with wildcards '*' permitted." +
                "\n\n    Find (ferret-out) one or more files, using GNU 'find' as the" +
                "\n    underlying file-search-engine.  File searching starts from your" +
                "\n    current/working directory (pwd), proceding recursively through" +
                "\n    lower subdirectories.  Use options --user, --path=SUBDIR, --home" +
                "\n    or --root to start at a different directory location without having" +
                "\n    without needing to 'cd' to that location." +
                "\n\n    Note that output is to StdOut (pipe-able), so output can be" +
                "\n    redirected to a file as needed.\n\n"
  opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
    $stdout.puts opts
    # $stdout.puts "«+»Additional Text«+»"
    options[:help] = true
    exit true
  end  # -? --help
}.parse!  # leave residue-args in ARGV

###############################
if options[:debug] >= DBGLVL3 #
  require 'pry'               #
  binding.pry                 #
end                           #
###############################

pp options if options[ :debug ] >= DBGLVL2

if ARGV[0]
  filetree = build_filetree( ARGV, start_at, options )
else
  STDERR.puts "%#{PROGNAME}-i-usage, $ #{PROGNAME} [options] filespec... [>outfile]"
  exit false
end

# Report (sub)header line when containing-path changes (like VMS DIR),
# then report basenames for each contained file in subdirectory:
report( filetree, options )

exit true
