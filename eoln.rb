#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# eoln.rb
#
# Copyright © 2019 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

# This command-utility is pipe-able as a stage-1 component only, outputs to StdOut;
# it does require an actual text-file (not StdIn), as that file must be opened and
# and (re)read twice, once for EONL detection, and once more for actual conversion.

# A tool to detect &/or convert ends-of-lines,
# e.g., <CR><LF> --> <LF> etc.
# This will handle all cases for Linux/Unix, Mac and Windows;
# someday, maybe address VMS text files of various sorts, too.
#
# Motivation:
#   This utility addresses a need to explicitly convert incompatible line-
#   endings which arise when transferring text files between and among diverse
#   operating systems with differing line-ending conventions. Errors, problems,
#   and misunderstandings about line-endings arise when (for example) a text
#   file is FTP'd from VMS to a Windows desktop, and then copied (perhaps via
#   SyncThing or a USB drive, etc.) to a Linux desk/laptop... That file will
#   have <CR><LF> line endings (due to the FTP transfer thru Windows).
#
#   Thus, the need for a tool to arbitrarily detect and optionally fix EOLN
#   separator characters for the general case.

# Some representative Ruby One-Liners --
#   (from http://reference.jumpingmonkey.org/programming_languages/ruby/ruby-one-liners.html)
#
# convert DOS newlines (CR/LF) to Unix format (LF)
# - strip newline regardless; re-print with unix EOL
#    $  ruby -ne 'BEGIN{$\="\n"}; print $_.chomp' < file.txt
#
# convert Unix newlines (LF) to DOS format (CR/LF)
# - strip newline regardless; re-print with dos EOL
#    $  ruby -ne 'BEGIN{$\="\r\n"}; print $_.chomp' < file.txt
#
# delete leading whitespace (spaces/tabs/etc) from beginning of each line
#    $  ruby -pe 'gsub(/^\s+/, "")' < file.txt
#
# delete trailing whitespace (spaces/tabs/etc) from end of each line
# - strip newline regardless; replace with default platform record separator
#    $  ruby -pe 'gsub(/\s+$/, $/)' < file.txt
#

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v1.1 (05/22/2019)"
  AUTHOR = "Lorin Ricker, Valley Center, California, USA"

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for binding.pry &/or pry-{byebug|nav} #
             ######################################################

require 'optparse'

  # watch for multi-toothpick "\\\\" trickiness here!
  LF = { :controlchar => "\n",   :printable => "\\n or «LF»", :doublecc => "\n\n" }
  CR = { :controlchar => "\r",   :printable => "\\r or «CR»", :doublecc => "\r\r" }
CRLF = { :controlchar => "\r\n", :printable => "\\r\\n or «CR»«LF»" }
LFCR = { :controlchar => "\n\r", :printable => "\\n\\r or «LF»«CR»" }
# This last, LFCR, is archaic, obsolete if ever used,
# included here simply for symmetry/completeness...
# -----

# def test_eoln( infspec, lineend, options )
# This variation reads the whole text file, counting possible line-ends,
# and must be done one-per-lineend-combination (for LF, CR, CRLF and LFCR).
# Inefficient!
#   lineend_count = 0
#   File.open( infspec, "r" ) do | inf |
#     lineend_pat = /[-#{lineend}]*  # zero or more non-lineend characters
#                    (#{lineend})+   # followed by one or more lineend character(s)
#                   /xm
#     pp lineend_pat if options[:debug] >= DBGLVL2
#     while line = inf.gets
#       m = lineend_pat.match( line )
#       lineend_count += 1 if m
#     end  # while
#   end  # File.open
#   return lineend_count > 0 ? lineend_count : false
# rescue Errno::ENOENT => e
#   fnf_error_exit( e, infspec)
# end  # test_eoln

def fnf_error_exit( e, infspec )
  STDERR.puts "%#{PROGNAME}-e-fnf, error opening file '#{infspec}'"
  exit false
end # fnf_error_exit

def test_eoln( infspec, options )
  found_eoln = nil
  lineend_pat = /([\n\r]{1,2})/  # either 1- or 2-chars: LF, CRLF, CR, LFCR
  m = lineend_pat.match( IO.read( infspec, 4096, 0, mode: "rb" ) )
  found_eoln = m.to_s if m   # the matched string
  pp lineend_pat, m, found_eoln if options[:debug] >= DBGLVL2
  return found_eoln
rescue Errno::ENOENT => e
  fnf_error_exit( e, infspec)
end # test_eoln

def detect_eoln( infspec, options )
  case test_eoln( infspec, options )
  when LF[:controlchar], LF[:doublecc]      # Unix/Linux/modern-Mac -- «LF»«LF» is a blank-line
    return LF
  when CRLF[:controlchar]                   # Windoze
    return CRLF
  when CR[:controlchar], CR[:doublecc]      # historical-Mac -- «CR»«CR» is a blank-line
    return CR
  when LFCR[:controlchar]                   # archaic, obsolete, for completeness only
    return LFCR
  else
    return false
  end
end # detect_eoln

def convert_file_eoln( infspec, inf_eoln, options )
  if options[:verbose] || options[:debug] >= DBGLVL2
    STDERR.puts " Input File Record Separator: #{inf_eoln[:printable]}"
    STDERR.puts "Output File Record Separator: #{options[:eoln][:printable]}"
  end
  $/ = inf_eoln[:controlchar]         #  $INPUT_RECORD_SEPARATOR or $RS
  $\ = options[:eoln][:controlchar]   # $OUTPUT_RECORD_SEPARATOR or $ORS
  # Copy input textfile to StdOut (which can be redirected),
  # handling EOLN separators for each...
  File.open( infspec, "r" ) do | inf |
    while ln = inf.gets
      ln = ln.chomp    #   trims $/ ($INPUT_RECORD_SEPARATOR or $RS)
      STDOUT.print ln  # appends $\ ($OUTPUT_RECORD_SEPARATOR or $ORS)
    end  # while
  end
rescue Errno::ENOENT => e
  fnf_error_exit( e, infspec)
end  #convert_file_eoln

# === Main ===
options = { :eoln       => LF,
            :detectonly => false,
            :verbose    => false,
            :debug      => DBGLVL0,
            :about      => false
          }

optparse = OptionParser.new { |opts|
  opts.on( "-e", "--eoln[=EOLN_KEYWORD]", /LFCR|CRLF|CR|LF|/i,
           "End-of-line character(s) to replace current EOLNs in outfile:",
           "LF (default), CR, CRLF or LFCR" ) do |val|
     options[:eoln] = case val.upcase.to_sym
                      when :LF then LF
                      when :CRLF then CRLF
                      when :CR then CR
                      when :LFCR then LFCR
                      end
  end  # -e --eoln
  opts.on( "-t", "--detect", "--detect-only",
           "Detect and report end-of-line separator(s), but do not fix" ) do |val|
    options[:detectonly] = true
  end  # -t --detect[-only]
  opts.separator ""
  # --- Verbose option ---
  opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
    options[:verbose] = true
  end  # -v --verbose
  # --- Debug option ---
  opts.on( "-d", "--debug", "=DebugLevel", Integer,
           "Show debug information (levels: 1, 2 or 3)",
           "  1 - enables basic debugging information",
           "  2 - enables advanced debugging information",
           "  3 - enables (starts) pry-byebug debugger" ) do |val|
    options[:debug] = val.to_i
  end  # -d --debug
  # --- About option ---
  opts.on_tail( "-a", "--about", "Display program info" ) do |val|
    $stdout.puts "#{PROGID}"
    $stdout.puts "#{AUTHOR}"
    options[:about] = true
    exit true
  end  # -a --about
  # --- Set the banner & Help option ---
  opts.banner = "\n  Usage: #{PROGNAME} [options] filename [ >outfilename ]" +
                "\n\n    where filename is the name (or pathname) of a text file." +
                "\n\n  Note that output is to StdOut (pipe-able), but can be redirected" +
                "\n  to a file as needed (likely the normal use-case)." +
                "\n\n  Example: `$ eoln --eoln=lf testCRLF.txt >testLF.txt` results in --" +
                "\n  (Windoze)                    (Linux/Unix)" +
                "\n  Text Line 1«CR»«LF»    ==>   Text Line 1«LF»" +
                "\n    Line 2«CR»«LF»               Line 2«LF»" +
                "\n    Line 3«CR»«LF»               Line 3«LF»" +
                "\n    ...«CR»«LF»                  ...«LF»" +
                "\n  «CR»«LF»                     «LF»\n\n"
  opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
    $stdout.puts opts
    # $stdout.puts "«+»Additional Text«+»"
    options[:help] = true
    exit true
  end  # -? --help
}.parse!  # leave residue-args in ARGV

###############################
if options[:debug] >= DBGLVL3 #
  require 'pry'               #
  binding.pry                 #
end                           #
###############################

pp options if options[ :debug ] >= DBGLVL2

if ARGV.count > 0  # ARGV[0] contains the name of the text-file to convert...
  inf_eoln = detect_eoln( ARGV[0], options )
  if inf_eoln
    if options[:detectonly]
      STDOUT.puts "%#{PROGNAME}-s-detected, EOLN: #{inf_eoln[:printable]}" if options[:detectonly]
    else
      if inf_eoln != options[:eoln]  # different EOLNs between input and output files?
        convert_file_eoln( ARGV[0], inf_eoln, options )
      else
        STDERR.puts "%#{PROGNAME}-w-noop, input and requested-output file line-endings are same, nothing to do..."
      end
    end
  else
    STDERR.puts "%#{PROGNAME}-e-no_eoln, no valid EOLN detected"
  end
  exit true
else
  STDERR.puts "%#{PROGNAME}-e-nofile, specify a text-file to convert"
  exit false
end
