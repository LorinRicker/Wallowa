#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# reparse.rb
#
# Copyright © 2018 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

# Template for a re-usable OptionParser in a kernel (global) method.
#
# Requires that the "constant" ARGV actually be mutable (via the
# array|hash #replace method), and that OptionParser.parse! can
# be called more than once.
#
# See: https://stackoverflow.com/questions/6712298/dynamic-constant-assignment
#       /53289491#53289491
#

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v0.02 (11/13/2018)"
  AUTHOR = "Lorin Ricker, Elbert, Colorado, USA"

# Modify the following template to include actual application help:
HelpBanner = <<EOHB

  Usage: #{PROGNAME} [options] [targetfile1] [targetfile2]...

          App-help description ........................ xxx
          xxx ......................................... xxx
          xxx ......................................... xxx
          xxx ......................................... xxx

EOHB

# Add application-specific options here:
app_options = {
    :foobar => false,
    :barfoo => false
    }

# Stamdard debug banner text, *rarely changed* -- an array of lines:
DebugBanner = [
  "Show debug information (levels: 1, 2 or 3)",
  "  1 - enables basic debugging information",
  "  2 - enables advanced debugging information",
  "  3 - enables (starts) pry-byebug debugger"
  ]

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for binding.pry &/or pry-{byebug|nav} #
             ######################################################
# -----

require 'optparse'
##require_relative 'lib/WhichOS'
require_relative 'lib/GetPrompted'

# Kernel (global) methods --

# OptionParser directly references the global com-line-argument array ARGV
# in its initialize method, and ARGV is mutable, so processing the first
# instance of ARGV and any subsequent altered/updated ARGV values is now
# implemented by this reparse method.
#
# Call as:
#    prgm_options = reparse( prgm_options )
# ...or, with default parameter hash:
#    options = reparse()
#
def reparse( newopts = options )
  optparse = OptionParser.new { |opts|
  # App-specific options here:

  # Standard (common) options here:
    # --- Force option ---
    opts.on( "-f", "--[no-]force", "--replace",
      "Force/replace any pre-existing symlink" ) do |val|
        newopts[:force] = val
      end  # -f --force --replace
    opts.separator ""
    # --- Dryrun option ---
    opts.on( "-n", "--[no-]dryrun", "--dry-run", "No-execute mode" ) do |val|
      newopts[:dryrun] = val
    end  # -n --dryrun
    # --- Verbose option ---
    opts.on( "-v", "--[no-]verbose", "--log", "Verbose mode" ) do |val|
      newopts[:verbose] = val
    end  # -v --verbose
    # --- Debug option ---
    opts.on( "-d", "--debug", "=DebugLevel", Integer,
             DebugBanner[0], DebugBanner[1],
             DebugBanner[2], DebugBanner[3] ) do |val|
      newopts[:debug] = val.to_i
    end  # -d --debug
    # --- About option ---
    opts.on_tail( "-a", "--about", "Display program info" ) do |val|
      $stdout.puts "#{PROGID}"
      $stdout.puts "#{AUTHOR}"
      newopts[:about] = true
      exit true
    end  # -a --about
    # --- Set the banner & Help option ---
    opts.banner = HelpBanner
    opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
      $stdout.puts opts
      # $stdout.puts "«+»Additional Text«+»"
      newopts[:help] = true
      exit true
    end  # -? --help
  }.parse!  # leaves non-options residue-args in ARGV for script to process

  return newopts
end  # reparse

# === Main ===
# Standard, common options for all applications:
std_options = {
    :verbose  => false,
    :debug    => DBGLVL0,
    :force    => false,
    :dryrun   => false,
    :about    => false
    }

options = app_options.merge( std_options )  # app_options + std_options
pp options
options = reparse( options )

###############################
if options[:debug] >= DBGLVL3 #
  require 'pry'               #
  binding.pry                 #
end                           #
###############################

options[:verbose] = true if options[:dryrun]
pp options

if options[:prompt] || ARGV.empty?
  pstr = PROGNAME.downcase
  # ...Prompt user for values, show running-tape of accumulated/calc'd time
  # display current interval as prompt> -- get user's input, no default answer:
  while comline = getprompted( pstr, "", false )
    break if comline == ""
    # Magic! The replace method replaces array|hash contents, even
    #        if the array is a "constant" like ARGV, suppressing
    #        the dumb "warning: already initialized constant ARGV"
    #        error message!
    ARGV.replace( comline.split )
    pp ARGV
    options = app_options.merge( std_options )  # app_options + std_options
    pp options
    options = reparse( options )
    pp options
  end  # while
end  # if options[:prompt]

exit true
