#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# fix-stdmusictitles.rb

# Use:
# $ cd $rips
# $ fix-stdmusictitles [debug]

# Copyright © 2021 Lorin Ricker <Lorin@RickerNet.us>
# Version 1.0, 11/24/2021

# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.

# PROGNAME = "fix-stdmusictitles"
#   PROGID = "#{PROGNAME} v1.0 (11/24/2021)"
#   AUTHOR = "Lorin Ricker, Valley Center, California, USA"

require 'fileutils'

direntries = Dir.glob( "*.ogg", File::FNM_DOTMATCH )
# Remove the back- and self- directory links
direntries.delete_if { | e | File.basename( e ) == '.' ||
  File.basename( e ) == '..'}
  
  direntries.each do | fspec |
    ##########################################################################################
    # This version is for target:
    #     01-Le rouge et le noir (Excerpts): mvmt-name.ogg
    # --> 01 - Teodorescu-Ciocănea - Le rouge et le noir - mvmt-name.ogg
    ##########################################################################################
  # Generic term fixes:
  tarfspec = fspec.gsub( /[ -][Mm]aj(or)?/, ' major' )
                  .gsub( /[ -][Mm]in(or)?/, ' minor' )
                  .gsub( /[Oo]p(\.)?/, 'Op' )
                  .gsub( /[Nn]o(\.)?/, 'Nr' )
                  .gsub( /[ -][Ss]harp/, '-sharp' )
                  .gsub( /[ -][Ff]lat/, '-flat' )
                  .gsub( ' in ', ', ' )

  # Either rename or debug-echo:  $ fix-stdmusictitles [debug]
  if ARGV[0] == nil
    FileUtils.mv( fspec, tarfspec )
  else
    puts "\nmv: \"#{fspec}\""
    puts "--> \"#{tarfspec}\""
  end
end

exit true
