#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# mklns.rb
#
# Copyright © 2018 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

# Creates a symbolic link to a target file using the file's fully-specified
# pathname, saving the symbolic link file to `~/Desktop/` by default, or to
# a specifically-named target-directory.

# Note:  Open-with or double-clicking the sym-link icon on the desktop
#        opens the application corresponding to the sym-link's file type
#        (extension), so name that sym-link with the same file extension
#        as the target file's type.

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v1.03 (10/09/2018)"
  AUTHOR = "Lorin Ricker, Elbert, Colorado, USA"

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for binding.pry &/or pry-{byebug|nav} #
             ######################################################
# -----

require 'optparse'

# === Main ===
options = { :verbose  => false,
            :debug    => DBGLVL0,
            :force    => false,
            :linkdir  => nil,
            :linkname => nil,
            :dryrun   => false,
            :about    => false
          }

optparse = OptionParser.new { |opts|
  # --- Force option ---
  opts.on( "-f", "--force", "--replace",
           "Force/replace any pre-existing symlink" ) do |val|
    options[:force] = true
  end  # -f --force --replace
  # --- Target Directory option ---
  opts.on( "-T LinkDirPath", "--linkdir", "--tardir", String,
           "Directory/path for the created symlink,",
           "default is your ~/Desktop/ directory" ) do |val|
    options[:linkdir] = val
  end  # -T --linkdir --tardir
  # --- Target Name option ---
  opts.on( "-L LinkName", "--linkname", "--tarname", String,
           "Filename for the created symlink,",
           "default is filename of linked/target",
           "file: targetfname1 (etc.)" ) do |val|
    options[:linkname] = val
  end  # -L --linkdir --tardir
  opts.separator ""
  # --- Dryrun option ---
  opts.on( "-n", "--dryrun", "--dry-run", "No-execute mode" ) do |val|
    options[:dryrun] = true
  end  # -n --dryrun
  # --- Verbose option ---
  opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
    options[:verbose] = true
  end  # -v --verbose
  # --- Debug option ---
  opts.on( "-d", "--debug", "=DebugLevel", Integer,
           "Show debug information (levels: 1, 2 or 3)",
           "  1 - enables basic debugging information",
           "  2 - enables advanced debugging information",
           "  3 - enables (starts) pry-byebug debugger" ) do |val|
    options[:debug] = val.to_i
  end  # -d --debug
  # --- About option ---
  opts.on_tail( "-a", "--about", "Display program info" ) do |val|
    $stdout.puts "#{PROGID}"
    $stdout.puts "#{AUTHOR}"
    options[:about] = true
    exit true
  end  # -a --about
  # --- Set the banner & Help option ---
  opts.banner = "\n  Usage: #{PROGNAME} [options] [targetfile1] [targetfile2]..." +
                "\n\n    Creates a symbolic link to a target file using" +
                "\n    the file's fully-specified pathname, saving the" +
                "\n    symbolic link file to `~/Desktop/` by default," +
                "\n    or to a specifically-named target-directory.\n\n"
  opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
    $stdout.puts opts
    # $stdout.puts "«+»Additional Text«+»"
    options[:help] = true
    exit true
  end  # -? --help
}.parse!  # leave residue-args in ARGV

###############################
if options[:debug] >= DBGLVL3 #
  require 'pry'               #
  binding.pry                 #
end                           #
###############################

options[:verbose] = true if options[:dryrun]

if ARGV.count > 0
  ARGV.each do | fn |

    target = File.expand_path( fn )  # the file to link-to...
    if ! File.exist?( target )
      $stderr.puts "%#{PROGNAME}-e-fnf, file #{target} not found"
      next
    end

    # ext  = File.extname( fn )
    base = File.basename( fn, ".*" )
    if ! options[:linkname]
      link = base  # Don't put a file-type/extension on the link itself;
                   # this allows the target file to determine Open-With app.
    else
      link = options[:linkname]
    end
    if ! options[:linkdir]
      linkname = File.join( "~", "Desktop", link )
    else
      linkname = File.join( options[:linkdir], link )
    end
    linkname = File.expand_path( linkname )

    cmd = "ln --symbolic \"#{target}\" \"#{linkname}\""
    cmd += " --verbose" if options[:verbose]
    cmd += " --force"   if options[:force]

    if ! options[:dryrun]
      result = %x{ #{cmd} }.strip
    else
      result = cmd  # just show the command-to-be-executed
    end
    $stderr.puts "%#{PROGNAME}-s-link, #{result}" if result != "" && options[:verbose]

  end  # ARGV.each
  exit true

else
  $stderr.puts "%#{PROGNAME}-e-insuff_args, no file argument found"
  exit false
end
