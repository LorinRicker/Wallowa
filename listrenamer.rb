#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# listrenamer.rb
#
# Copyright © 2022-2023 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v2.1 (02/15/2023)"
  AUTHOR = "Lorin Ricker, Valley Center, California, USA"

#  CONFIGDIR = File.join( ENV['HOME'], ".config", PROGNAME )
#  CONFIGFILE = File.join( CONFIGDIR, "#{PROGNAME}.yaml.rc" )
# -----

require 'optparse'    # standard library
require_relative 'lib/FileEnhancements'  # includes AppConfig class
require_relative 'lib/CLU'

include CLU

COMMENT_CHAR = '#'
EXECUTE_CHAR = '$'
CD_CHAR      = '>'
SPLIT_CHAR   = '|'

options = { :noop     => false,
            :sudo     => "",
            :update   => false,
            :verbose  => false,
            :debug    => DBGLVL0,
            :prydebug => DBGLVL0,
            :about    => false
          }

# Turn on/off Config-File option (default is off)
options.merge!( { :configfile => false } )  # change to true to enable...
# Add-in Common Options (--help, --about, --dryrun, --sudo,
#                        --verbose, --debug, --pry )
options.merge!( CLU.common_options )

def oparse( options )
  optparse = OptionParser.new { |opts|
    # opts.on( "-«+»", "--«+»",  # "=«+»", String,
    #          "«+»" ) do |val|
    #   options[:«+»] = «+»
    # end  # -«+» --«+»
    opts.on( "-S", "--sudo",
            "Run this backup/restore with sudo" ) do |val|
      options[:sudo] = "sudo"
    end  # -S --sudo
    opts.on( "-n", "--noop", "--dryrun", "--test",
            "Dry-run (test & display, no-op) mode" ) do |val|
      options[:noop]  = true
      options[:verbose] = true  # Dry-run implies verbose...
    end  # -n --noop
    # --- Verbose option ---
    opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
      options[:verbose] = true
    end  # -v --verbose
    # --- Debug option ---
    opts.on( "-D", "--debug", "=DebugLevel", Integer,
            "Show debug information (levels: 1, 2 or 3)",
            "  1 - enable basic debug information output",
            "  2 - enable advanced debug information output",
            "  3 - enable (starts) 'Ruby debug' gem debugger" ) do |val|
      options[:debug] = val.to_i
    end  # -D --debug
    opts.on( "-P", "--pry", "Debug with pry (rather than 'Ruby debug' gem)") do |val|
      options[:prydebug] = true
      options[:debug] = DBGLVL3
    end  # -P --pry
    # --- About option ---
    opts.on_tail( "-a", "--about", "Display program info" ) do |val|
      $stdout.puts "#{PROGID}"
      $stdout.puts "#{AUTHOR}"
      options[:about] = true
      exit true
    end  # -a --about
    # --- Set the banner & Help option ---
    opts.banner = "\n  Usage: #{PROGNAME} [options] renamelist" +
                  "\n\n   where renamelist is a file as input\n\n" +
                  ""
    opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
      $stdout.puts opts
      # $stdout.puts "«+»Additional Text«+»"
      options[:help] = true
      exit true
    end  # -? --help
  }.parse!  # leave residue-args in ARGV
end

# Parse Command Line Options
oparse( options )

# === Debugging starts here, if selected from Command Line...
CLU.debuggerRequired?( options[:debug], options[:prydebug] )

# === Main ===

# f1 = ARGV[0] || ""  # a completely empty args will be nil here, ensure "" instead
# f2 = ARGV[1] || ""

begin
  lines = File.open( ARGV[0], "r" ).readlines
rescue Errno::ENOENT => e
  $stderr.puts "%#{PROGNAME}-f-fnf, can't open #{ARGV[0]}"
  exit false
end

lines.each do | line |
  line = line.chomp
  next if line.strip == ''  # skip blank lines
  case line[0]
  when COMMENT_CHAR  # skip any comment lines
    next
  when EXECUTE_CHAR  # execute a whole bash command
    cmd = line[1..-1].strip
    if ! options[:noop]
      %x{ "#{cmd}" }
    else
      puts "#{PROGNAME}-i-eval, --> \"#{cmd}\""
    end
  when CD_CHAR  # cd-shorthand
    tardir = line[1..-1].strip
    if ! options[:noop]
      Dir.chdir( tardir )
      puts Dir.pwd
    else
      puts "#{PROGNAME}-i-cd, --> \"#{tardir}\""
    end
  else      # the rename itself...
    # rename source-file/dir to destination file/dir
    src, dst = line.split( SPLIT_CHAR )
    src = File.expand_path( src.strip )
    dst = File.expand_path( dst.strip )
    if ! options[:noop]
      FileUtils.mv( "#{src}", "#{dst}", **{ :verbose => true } )
    else
      puts "#{PROGNAME}-i-rename, \"#{src}\" --> \"#{dst}\""
    end
  end  # case
end

exit true
