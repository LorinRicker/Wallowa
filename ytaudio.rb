#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# ytaudio.rb
#
# Copyright © 2018-2019 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

# Extract audio from YouTube videos using youtube-dl.
#
# use: $ ytaudio format out-filename YouTube-id
# where:
#    $1 - output filename
#    $2 - YouTube video id: https://www.youtube.com/watch?v=XXXXXXXXXXXX
#    $3 - audio format: vorbis (default), mp3...
#
# See also $com/Install-youtube-dl.sh ...
#   and https://github.com/rg3/youtube-dl/ and https://yt-dl.org/
#
# alias   ytdla='youtube-dl -x --audio-format vorbis -o'
# alias ytdlmp3='youtube-dl -x --audio-format mp3 -o'
# alias ytdlogg='youtube-dl -x --audio-format vorbis -o'

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v1.7 (12/10/2019)"
  AUTHOR = "Lorin Ricker, Elbert, Colorado, USA"

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for binding.pry &/or pry-{byebug|nav} #
             ######################################################

YTIDLEN  = 11
YTURL    = "https://www.youtube.com/watch?v="
HTTPS    = "https://"
HTTPSLEN = HTTPS.length

require 'optparse'  # standard library
require 'open3'
require_relative 'lib/ANSIseq'

# === Main ===
options = { :verbose => false,
            :debug   => DBGLVL0,
            :dryrun  => false,
            :about   => false
          }

optparse = OptionParser.new { |opts|
  # Install youtube-dl directly -- instructions from https://youtube-dl.org:
  #    curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
  #    sudo chmod a+rx /usr/local/bin/youtube-dl
  # or:
  #    sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl
  #    sudo chmod a+rx /usr/local/bin/youtube-dl
  opts.on( "-i", "--install",  "Install youtube-dl" ) do |val|
    curl_result = %x{ whereis curl }  # test if curl is available...
    if curl_result != 'curl:\n'       # use literal '\n', not really <lf>
      cmd = "sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl"
    else
      wget_result = %x{ whereis wget }  # no curl?  try if wget is available...
      if wget_result != 'wget:\n'       # use literal '\n', not really <lf>
        cmd = "sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl"
      else
        $stderr.puts "%#{PROGNAME}-e-cannot_install, no curl or wget, cannot install youtube-dl"
        exit false
      end
    end
    $stderr.puts "%#{PROGNAME}-i-install, installing youtube-dl..."
    result = %x{ #{cmd} }
    %x{ sudo chmod a+rx /usr/local/bin/youtube-dl }
    $stderr.puts "%#{PROGNAME}-s-done, youtube-dl install done: #{result}"
    exit true
  end  # -u --update
  # Update youtube-dl directly:
  opts.on( "-u", "--update",  "Update youtube-dl" ) do |val|
    $stderr.puts "%#{PROGNAME}-i-update, updating youtube-dl..."
    result = %x{ sudo youtube-dl --update }
    $stderr.puts "%#{PROGNAME}-s-done, update done: #{result}"
    exit true
  end  # -u --update
  opts.separator ""
  # --- Verbose option ---
  opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
    options[:verbose] = true
  end  # -v --verbose
  opts.on( "-n", "--dryrun", "Dry run: don't actually download files" ) do |val|
    options[:dryrun] = true
  end  # -n --dryrun
  # --- Debug option ---
  opts.on( "-d", "--debug", "=DebugLevel", Integer,
           "Show debug information (levels: 1, 2 or 3)",
           "  1 - enables basic debugging information",
           "  2 - enables advanced debugging information",
           "  3 - enables (starts) pry-byebug debugger" ) do |val|
    options[:debug] = val.to_i
  end  # -d --debug
  # --- About option ---
  opts.on_tail( "-a", "--about", "Display program info" ) do |val|
    $stdout.puts "#{PROGID}"
    $stdout.puts "#{AUTHOR}"
    options[:about] = true
    exit true
  end  # -a --about
  # --- Set the banner & Help option ---
  opts.banner = "\n  Usage: #{PROGNAME} [options] \"output_audio.ogg\" \"YouTubeAudioTag\"" +
                "\n\n    Extracts audio from YouTube videos." +
                "\n    Provide outfile name in double-quotes; file type is defaulted.\n" +
                "\n    Specify any part of the YouTube video-URL that you can cut&paste," +
                "\n    as long as that fragment includes the actual video-id (11-character" +
                "\n    tag) as highlighted here:" +
                "\n\n      https://www.youtube.com/watch?v=" + "12345678911".underline.color(:cyan) +
                "\n\n    Vorbis (.ogg) is the default audio-format.\n\n"
  opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
    $stdout.puts opts
    # $stdout.puts "«+»Additional Text«+»"
    options[:help] = true
    exit true
  end  # -? --help
}.parse!  # leave residue-args in ARGV

pp options if options[:verbose]

###############################
if options[:debug] >= DBGLVL3 #
  require 'pry'               #
  binding.pry                 #
end                           #
###############################

if ARGV.count == 0
  # display help text
  exit true
end

oname = ARGV[0]  # output filename

vurl = ARGV[1]
# Mung together the whole YouTube URL, even if user's cut&paste is partial,
# like: "s://www.youtube.com/watch?v=GdPW8oMwjoY" or "/watch?v=blRX58LOEMI"
#       or "?v=dyX96IRpipQ" or "=" -- the minimum is, of course, the video-
#       id string itself: "SZxp8O_Lvyg"
if vurl[0,HTTPSLEN] != HTTPS
  if vurl.include?('=')
    vurl = YTURL + vurl.split('=')[1]   # use the part to-the-right of '='
  else
    if vurl.length == YTIDLEN
      vurl = YTURL + vurl
    else
      $stderr.puts "%#{PROGNAME}-e-badYTid, invalid (too short) YouTube tag/id \"#{vurl}\""
      exit false
    end
  end
# else the whole YouTube URL (https://...) has been provided... at least try it...
end

aformat, ext = ARGV[2] || "vorbis", ".ogg"

xstat = 0
cmd = "youtube-dl --extract-audio --audio-format #{aformat}" \
      + " --output \"#{oname}.%(ext)s\" #{vurl}"             \
      + " && file #{oname}#{ext}"
$stderr.puts "%#{PROGNAME}-i-echo, $ #{cmd}" if options[:verbose] || options[:dryrun]
if !options[:dryrun]   # do it if not a dry-run...
  Open3.popen2e( cmd ) do | stdin, stdouterr, thrd |
    stdouterr.each { |ln|
      $stdout.puts "| #{ln}"
    }
    xstat = thrd.value.exitstatus  # Process::Status
  end
end

exit xstat
