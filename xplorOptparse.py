#!/usr/bin/env python
# -*- encoding: utf-8 -*-

# xplorOptparse.py
#
# Copyright © 2018-2020 Lorin Ricker <Lorin@RickerNet.us>
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.

# Purpose: A Python script set up to easily explore the optparse module
#          and the OptionParser class (which is nearly inscrutable when
#          just looking at the source code).

from optparse import OptionParser
import sys
import os

PROGNAME = os.path.basename( sys.argv[0] ).split('.')
PROGID   = "%s v0.2 (01/28/2020)" % PROGNAME[0]
AUTHOR   = "Lorin Ricker, Valley Center, California, USA"

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for some-Python-Debug (TBD)           #
             ######################################################
# -----

def about_program( programid, coder, exitif ):
    print( "%s ...on Python v%s" % (programid, PYTHON_VERSION) )
    print( "%s" % coder )
    if exitif:
        sys.exit(0)     # just terminate programid
    else:
        return( true )

def call_about( option, optstr, value, parser, *args, **kwargs ):
    about_program( PROGID, AUTHOR, true )

def main():
    parser = OptionParser()

    parser.set_defaults( testopt = None,
                         verbose = False,
                         debug   = DBGLVL0,
                         about   = False )

    # --- Test option ---
    parser.add_option( "-t", "--testopt", metavar="VALUE",
                       action="store", type="string", dest="testopt",
                       help="Test Option" )
    # --- Verbose option ---
    parser.add_option( "-v", "--verbose",
                       action="store_true", dest="verbose",
                       help="Verbose mode" )
    # --- Debug option ---
    parser.add_option( "-d", "--debug", metavar="DEBUGLEVEL",
                       action="store", type="int", dest="debuglevel",
                       help="Show debug information (levels: 1, 2 or 3)"
                       "  1 - enables basic debugging information"
                       "  2 - enables advanced debugging information"
                       "  3 - enables (starts) pry-byebug debugger" )
    # --- About option ---
#    parser.add_option( "-a", "--about",
#                       action="callback", callback="call_about" )
                       # callbacks cannot specify a help="string" )
    # --- Where is "Set the banner & Help option"? ---
    #     In Python's OptionParser, it's built-in.

    ( options, arguments ) = parser.parse_args()

    # Some code to explore/step-through:
    print( "The options parsed from com-line:" )
    print options
    print( "And the arguments after separating the options:")
    print arguments

    sys.exit(0)

if __name__ == "__main__":
    main()
