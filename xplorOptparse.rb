#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# xplorOptparse.rb
#
# Copyright © 2018-2020 Lorin Ricker <Lorin@RickerNet.us>
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.

# Purpose: A Ruby script set up to easily explore the optparse gem and
#          the OptionParser class (which is nearly inscrutable when just
#          looking at the source code).
#   Tools: Gem resources include pry (binding.pry) for step-thru debugging,
#          and method_source for examining source code.

require "method_source"
  #
  # enables: op = OptionParser.new
  #          op.method(:amethod).source.display
  # or even: Child.new.method(:bmethod).super_method.source.display
  #
  # See Ruby Tapas episode #540 - "Ruby Spelunking" for more info & technique
  #

require 'pry'               # do this early to enable OptionParser exploration
binding.pry                 #

# ----------------------------
# Initial pry breakpoint here:
# ----------------------------

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v0.2 (01/28/2020)"
  AUTHOR = "Lorin Ricker, Valley Center, California, USA"

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for binding.pry &/or pry-{byebug|nav} #
             ######################################################
# -----

require 'optparse'

options = { :testopt    => nil,
            :verbose    => false,
            :debug      => DBGLVL0,
            :about      => false
          }

optparse = OptionParser.new { |opts|
  # --- Test option ---
  opts.on( "-t", "--testopt=VALUE", String, "Test Option" ) do |val|
    options[:testopt] = val
  end  # -t --testopt
  # --- Verbose option ---
  opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
    options[:verbose] = true
  end  # -v --verbose
  # --- Debug option ---
  opts.on( "-d", "--debug", "=DebugLevel", Integer,
           "Show debug information (levels: 1, 2 or 3)",
           "  1 - enables basic debugging information",
           "  2 - enables advanced debugging information",
           "  3 - enables (starts) pry-byebug debugger" ) do |val|
    options[:debug] = val.to_i
  end  # -d --debug
  # --- About option ---
  opts.on_tail( "-a", "--about", "Display program info" ) do |val|
    require_relative 'lib/AboutProgram'
    options[:about] = about_program( PROGID, AUTHOR, true )
  end  # -a --about
  # --- Set the banner & Help option ---
  opts.banner = "\n  Usage: #{PROGNAME} [options] Display terminal characteristics\n\n"
  opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
    $stdout.puts opts
    exit true
  end  # -? --help
}.parse!  # leave residue-args in ARGV

# ###############################
# if options[:debug] >= DBGLVL3 #  <-- don't need this here...
#   require 'pry'               #
#   binding.pry                 #
# end                           #
# ###############################

# Some code to explore/step-through:
options.each { | key, val |
  val ||= "NIL!"
  puts "options -- key: '#{key}' -- value: '#{val}'"
}

exit true
