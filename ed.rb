#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# ed.rb
#
# Copyright © 2019-2021 Lorin Ricker <Lorin@RickerNet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.

# Provide advanced text editor command line capabilities, including:
#    * Stash and reuse '.cf' (current filespec) on a per-project
#      or per-directory basis
#    * Use of a default editor, like atom
#    * Selection of any installed alternate editor (emacs, vim, etc.)
#
# (Replaces any command-line alias 'ed')
#
# Note:  'gksudo' is no longer supported, fully deprecated by nearly all
#        Linux distros, including Ubuntu.
# To edit any system (text) file as root:
#    1. $ sudo apt install -y nautilus-admin
#    2. $ export SUDO_EDITOR='gedit'  #or 'nano'; see $lgi/bashrc
#    3. Restart Nautilus
#    4. Right-click a system/text file, choose "Edit as Administrator"
#

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v3.0 (11/24/2021)"
  AUTHOR = "Lorin Ricker, Valley Center, California, USA"

AVAILABLE_EDITORS = /atom|code|emacs|vim|nano|gedit|kate|joe|geany/i
DEFAULT_EDITOR    = File.basename( ENV["EDITOR"] )  # atom, emacs, etc.
WINDOW_EDITORS    = /atom|code|emacs|kate|geany/i
TERMINAL_EDITORS  = /vim|nano|joe|gedit/i

# Online Documentation & Help Pages:
# Atom (GitHub):      https://atom.io/docs
# VScode (Microsoft): https://code.visualstudio.com/docs/
# emacs (Gnu):        https://www.gnu.org/software/emacs/manual/
# vim:                https://www.vim.org/docs.php
# nano:               https://www.nano-editor.org/docs.php
# gedit:              https://help.gnome.org/users/gedit/stable/
# kate (KDE):         https://docs.kde.org/stable5/en/applications/kate/
# joe:                https://joe-editor.sourceforge.io/4.6/man.html
# geany:              https://www.geany.org/

# Most editors (GUIs) generate a crap-load of startup messages and errors,
# none of which the user can do anything about, dump'em' to the bit-bucket:
DISCARD_ERRORS = " 2>/dev/null".freeze

# Note: nano will throw "Too many errors from stdin" if attempted
# to run in background!  This warning-note is duplicated below...
IN_BACKGROUND     = " &"

SPC = ' '.freeze
DQUOTE = '"'.freeze
STRIP_FILE_EXTENSION = '.*'.freeze

# "Current File" contains the file specification information
#    for the "current file edited"...
CF = './.cf'.freeze
CF_PREFIX = ( CF.split('/')[1] + '_' ).freeze

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for binding.pry &/or pry-{byebug|nav} #
             ######################################################

require 'optparse'

# -----

def check( cfile )
  # Open the .cf file and read its first line for filespec to edit;
  #   if that file does not exist in this directory, then simply open
  #   the text editor as a "no file specified" (empty) session.
  textfile, editor = nil  # initialize...
  $stderr.puts "%#{PROGNAME}-i-cf_override, using #{cfile} instead of #{CF}" if cfile != CF
  begin
    pat1 = /^:textfile: *'([^']+)'/   # e.g.  :textfile: '/home/lorin/ezekiel.rb'
                                      #  and  :textfile: ''  but (empty) is invalid
    pat2 = /^:editor: *'([^']+)'/     # e.g.  :editor: 'atom'
    File.open( cfile, 'r' ) do | inf |
      while !inf.eof?
        ln = inf.gets.chomp   # Don't skip this assignment, as trying to
                              # test the inf.gets-buffer directly against
                              # the pattern 'pat' is unreliable.
        textfile = $1 if ln =~ pat1
        editor   = $1 if ln =~ pat2
      end  # while
    end  # File.open()
  rescue SystemCallError => e
    # Expect: No such file or directory @ rb_sysopen - ./.cf (Errno::ENOENT)
    #   if File-Not-Found (FNF).
    # It's OKAY if the .cf file does not exist (cannot be opened);
    # just continue with an empty textfile-spec and default editor...
    textfile = nil
    editor   = nil
  end
  [ textfile, editor ]
end  # check

def locate( editor )
  editorpgm = %x{ whereis #{editor} }.split(SPC)[1]
  # %x{ whereis atom }    ==> atom: /usr/bin/atom /usr/share/atom
  #   so ...split(SPC)[1] ==> /usr/bin/atom
  # but...
  # %x{ whereis fooxed }  ==> fooxed:
  #   so ...split(SPC)[1] ==> nil
  if !editorpgm
    $stderr.puts "%#{PROGNAME}-e-editor_not_found, text editor \"#{editor}\" not installed"
    exit false
  end
  editorpgm
end  # locate

def no_readonly( editor )
  $stderr.puts "%#{PROGNAME}-w-no_readonly, #{editor.capitalize} does not support read-only mode"
end  # no_readonly

def readonly_option( options )
  cmd = SPC
  case options[:editor].to_sym
  when :atom, :code, :emacs, :gedit, :kate
    # atom has no read-only mode...   See: $ atom --help
    # code has no read-only mode...   See: https://code.visualstudio.com/docs/editor/command-line
    # emacs has no read-only mode...  See: $ man emacs (etc.)
    # kate does not appear to have a read-only mode... See:
    #   https://docs.kde.org/stable5/en/applications/kate/fundamentals.html#starting-from-the-command-line
    no_readonly( options[:editor] ) if options[:readonly]
  when :gedit
    # gedit has no read-only mode...  See: $ man gedit
    no_readonly( options[:editor] ) if options[:readonly]
    cmd += "--new-window"
  when :vim
    cmd += "-R" if options[:readonly]
  when :nano
    # See:  $ man nano
    cmd += "--view" + SPC if options[:readonly]
    cmd += "--tabstospaces --multibuffer --trimblanks --atblanks --linenumbers"
  when :joe
    # Joe's command-line qualifiers are a bit weird:  One "-" enables,
    # but two "--" disables an option/qualifier.
    cmd += "-rdonly" if options[:readonly]       # yes, just one "-"
  when :geany
    cmd += "--read-only" if options[:readonly]
  else
    $stderr.puts "%#{PROGNAME}-e-no_such_editor, requested editor is not available/installed"
    exit false
  end  # case
  cmd
end  # readonly_option

def file_argument( textfile, options )
  cmd  = SPC + DQUOTE + textfile + DQUOTE
  cmd += SPC + DISCARD_ERRORS unless options[:debug] >= DBGLVL1
  # need a specific return value here, don't assume it'll be cmd value above...
  cmd
end  # file_argument

def rewrite( textfile, options, cmd, timestamp )
  File.open( options[:cf], 'w', perm: 0o600, textmode: true ) do | outf |
    pathspec = File.dirname( textfile )
    filespec = File.basename( textfile )
    filename = File.basename( filespec, STRIP_FILE_EXTENSION )
    outf.puts ":textfile:  '#{textfile}'"
    outf.puts ":pathspec:  '#{pathspec}'"
    outf.puts ":filespec:  '#{filespec}'"
    outf.puts ":filename:  '#{filename}'"
    outf.puts ":editor:    '#{options[:editor]}'"
    outf.puts ":fullcmd:   '#{cmd}'"
    outf.puts ":timestamp: '#{timestamp.strftime("%Y-%m-%d %H:%M:%S")}'"
  end  # File.open
end  # rewrite

def execute( options, cmd )
  if options[:editor] =~ TERMINAL_EDITORS
    cmd += IN_BACKGROUND if options[:editor] != 'nano'
    # nano will throw "Too many errors from stdin" if attempted to run in background!
    ok = system( cmd )  # terminal-based editors must execute syncronously in this process
    $stderr.puts "%#{PROGNAME}-i-oops, #{editor} did not start" if !ok
  else
    pid = ok = spawn( cmd )  # windows-based editors can return immediately
    $stderr.puts "%#{PROGNAME}-i-pid, text editor process-id is: #{pid}" if options[:verbose]
  end
  ok
end  # execute

def show_version( options )
  cureditor = locate( options[:editor] )
  cmd  = cureditor + ' --version'
  if ( options[:editor] == 'code' )  # Visual Studio Code has an extra trick
    cmd += "\n" + cureditor + ' --list-extensions'
  end
  system( cmd )
end  # show_version

# -----

options = { :cf       => CF,
            :editor   => nil,
            :readonly => false,    # read-only, read-write
            :skipcf   => false,
            :dropcf   => false,
            :showvers => false,
            :verbose  => false,
            :debug    => DBGLVL0,
            :about    => false
          }

optparse = OptionParser.new { |opts|
  opts.on( "-c", "--cf=ALTFILENAME",
          "Alternate cf-file (an auxiliary/alternate to the '.cf' file);",
          "this altername filename is always prefixed with '.cf_' as a",
          "convention; you can specify either 'ALTNAME' or '.cf_ALTNAME'" ) do |val|
    baseval = File.basename( File.expand_path( val ) )
    pathval = File.dirname( val )
    options[:cf] = File.join( pathval,
                              ( baseval[0..3] == CF_PREFIX ?
                                   baseval : CF_PREFIX + baseval ) )
  end  # -c --cf
  opts.on( "-e", "--editor=EDITOR",
           "Text editor (Atom, code, Emacs, vim, nano, gedit, etc.); default",
           "is '#{ENV["EDITOR"]}' (your own ENV['EDITOR'] environment variable)",
    AVAILABLE_EDITORS ) do |val|
      options[:editor] = val.downcase
  end  # -e --editor
  opts.on( "-s", "--skip",
           "Skip, do not read cf-file for this edit command" ) do |val|
    options[:skipcf] = true
  end  # -s --skip
  opts.on( "-n", "--drop",
           "Drop, do not save cf-file data for this edit command" ) do |val|
    options[:dropcf] = true
  end  # -n --drop
  opts.on( "-l", "--version",
           "Show current editor version info" ) do |val|
    options[:showvers] = true
  end  # -l --version
  opts.on( "-r", "--read=MODE", /write|only/i,
           "Edit mode: read-WRITE (default) or read-ONLY" ) do |val|
      options[:readonly] = val.downcase == "only"  # true for read-only, false otherwise
  end  # -r --read
  opts.separator "\n"
  # --- Verbose option ---
  opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
    options[:verbose] = true
  end  # -v --verbose
  # --- Debug option ---
  opts.on( "-d", "--debug", "=DebugLevel", Integer,
           "Show debug information (levels: 1, 2 or 3)",
           "  1 - enables basic debugging information",
           "  2 - enables advanced debugging information",
           "  3 - enables (starts) pry-byebug debugger" ) do |val|
    options[:debug]   = val.to_i
    options[:verbose] = true
  end  # -d --debug
  # --- About option ---
  opts.on_tail( "-a", "--about", "Display program info" ) do |val|
    require_relative 'lib/AboutProgram'
    options[:about] = about_program( PROGID, AUTHOR, true )
  end  # -a --about
  # --- Set the banner & Help option ---
  opts.banner = "\n  Usage: #{PROGNAME} [options] [ textfile ]" +
                "\n\n    Edit-a-text-file wrapper: invoke any common text editor" +
                "\n    (see list below) from a single command." +
                "\n\n    If textfile is missing (omitted), then the .cf file (or its" +
                "\n    alternative specified by -c or --cf=CF_FILE) is read to obtain" +
                "\n    the 'current file' to edit.  The .cf file (itself a simple text" +
                "\n    file; you can cat it to see its contents) is always refreshed" +
                "\n    with the latest (last) textfile (filespec) provided by the most" +
                "\n    recent ed command." +
                "\n\n    Exit/Quit shortcuts:" +
                "\n      Atom   -- Ctrl/Q      Emacs -- Ctrl/X Ctrl/C" +
                "\n      VScode -- Ctrl/Q        vim -- :q or :q! or :wq <Enter>" +
                "\n      gedit  -- Ctrl/Q       nano -- Ctrl/X" +
                "\n      Kate   -- Ctrl/Q        Joe -- Ctrl/K Q" +
                "\n      Geany  -- Ctrl/Q\n\n"
  opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
    $stdout.puts opts
    options[:help] = true
    exit true
  end  # -? --help
}.parse!  # leave residue-args in ARGV

###############################
if options[:debug] >= DBGLVL3 #
  require 'pry'               #
  binding.pry                 #
end                           #
###############################

pp options if options[:verbose]

# Open/check the .cf file for
# :textfile:, last file edited, and
# for :editor:, last editor used --
textfile, editor = check( options[:cf] ) if !options[:skipcf]
# Use editor user specified on cmd-line, else use last-saved in .cf, else default:
options[:editor] ||= editor
options[:editor] ||= DEFAULT_EDITOR

if options[:showvers]
  show_version( options )
else
  # Use only the first cmd-line argument, discard any ARGV[2], ...
  textfile = File.expand_path( ARGV[0] ) if ARGV.length >= 1

  # Build the command string:
  cmd = locate( options[:editor] )   \
        + readonly_option( options ) \
        + file_argument( textfile, options )
  $stderr.puts "%#{PROGNAME}-i-echo, cmd: \"#{cmd}\"" if options[:verbose]

  # Update the .cf file if/as needed:
  rewrite( textfile, options, cmd, Time.now ) if !options[:dropcf] && textfile != ""
  # Execute the editor command:
  execute( options, cmd )
end
exit true
