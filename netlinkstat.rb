#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# netlinkstat.rb
#
# Copyright © 2021-2025 Lorin Ricker <Lorin@RickerNet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.

# See extended documentation, including derivation, at end-of-this-file.

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v0.23 (02/13/2025)"
  AUTHOR = "Lorin Ricker, Valley Center, California, USA"

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for binding.pry &/or pry-{byebug|nav} #
             ######################################################

# current 12-17-2021 - Valley Center Wireless
MYEXTIP = "76.194.191.199"

SUCCESS     =  0
INDENT      = 10
SPACE       = ' '.freeze
FAILMARK    = '|'.freeze
FAILINDENT  = ( "\n" + SPACE * INDENT + FAILMARK ).freeze
SEPARATOR   = '='.freeze
SLEEPFILTER = 3  # filter out intermittent/quick drops...

# -----

require 'optparse'    # standard library
require 'date'
require 'fileutils'
require_relative 'lib/ANSIseq'
require_relative 'lib/Combinatorics'
require_relative 'lib/TermChar'
require_relative 'lib/TimeInterval'

# Don't need a *.config file for this app, but will use this
# configuration directory for log-file storage:
CONFIGDIR = File.join( ENV['HOME'], ".config", PROGNAME )

# ==========

def test_or_create( dirfile )
  FileUtils.mkdir( dirfile, :mode => 0700 ) if ! File.exist?( dirfile )
end  # test_or_create


def init_nodelist( options )
  nl = Array.new
  nl = [ { name: 'Google DNS Server#1',       ipaddr: '8.8.8.8' },
         { name: 'Google',                    ipaddr: 'google.com' },
         { name: 'Cloudflare DNS Resolver#0', ipaddr: '1.0.0.1' },
         { name: 'Ubuntu',                    ipaddr: 'ubuntu.com' },
         { name: 'Google DNS Server#2',       ipaddr: '8.8.4.4' },
         { name: 'Wikipedia',                 ipaddr: 'wikipedia.com' },
         { name: 'Cloudflare DNS Resolver#1', ipaddr: '1.1.1.1' },
         { name: 'Ruby Language',             ipaddr: 'ruby-lang.com' },
         { name: 'Free Software Foundation',  ipaddr: 'fsf.org' },
         { name: 'SlashDot',                  ipaddr: 'slashdot.org' },
         { name: 'Substack',                  ipaddr: 'substack.com' },
         { name: 'Lua',                       ipaddr: 'lua.org' },
         { name: 'PARSEC Group',              ipaddr: 'parsec.com' },
         { name: 'Pop!_OS',                   ipaddr: 'pop.system76.com' },
         { name: 'Proton',                    ipaddr: 'proton.me' },
         { name: 'Python',                    ipaddr: 'python.com' },
         { name: 'Amazon',                    ipaddr: 'amazon.com' },
         { name: 'GitLab',                    ipaddr: 'gitlab.com' },
         { name: 'GitHub',                    ipaddr: 'github.com' },
         { name: 'CrashPlan',                 ipaddr: 'crashplanpro.com' },
         { name: 'Red Hat',                   ipaddr: 'redhat.com' },
         { name: 'AT&T',                      ipaddr: 'att.com' },
         { name: 'Linux',                     ipaddr: 'linux.org' },
         { name: 'Gnu',                       ipaddr: 'gnu.org' },
         { name: 'TruthSocial',               ipaddr: 'truthsocial.com' },
         { name: 'IEEE',                      ipaddr: 'ieee.org' },
         { name: 'TechRepublic',              ipaddr: 'techrepublic.com' },
         { name: 'LinkedIn',                  ipaddr: 'linkedin.com' },
         { name: 'OpenID',                    ipaddr: 'openid.stackexchange.com' },
         { name: 'StackOverflow',             ipaddr: 'stackoverflow.com' },
         { name: 'Linux Foundation',          ipaddr: 'linuxfoundation.org' },
         { name: 'Fox News',                  ipaddr: 'foxnews.com' },
         { name: 'Spectrum/IEEE',             ipaddr: 'spectrum.ieee.org' },
         { name: 'Southwest Air',             ipaddr: 'southwest.com' },
         { name: 'Atlassian/Jira',            ipaddr: 'atlassian.com' },
         { name: 'YouTube',                   ipaddr: 'youtube.com' },
         { name: 'X.com',                     ipaddr: 'x.com' },
         { name: 'bitly',                     ipaddr: 'bitly.com' },
         ]
  $stdout.puts "Nodelist count: #{nl.length}" if options[:debug] >= 1
  return nl
end  # init_nodelist

def init_intlist( pinginterval )
  intlist = Array.new
  n = 2
  term = 0
  # create an interval-list consisting of Fibonacci(n) for n >= 2, up to pinginterval
  # thus:  [ 2, 3, 5, 8, 13, 21, 34, 55 ] when pinginterval is 60 (default)
  #        [ 2, 3, 5, 8, 13, 21, 34, 55, 89 ] when pinginterval is 90 or 120; etc.
  until term >= pinginterval
    term = Combinatorics.fibonacci( n )
    intlist[n-2] = term if term < pinginterval
    n += 1
  end  # until
  return intlist
end  # init_intlist

def logmessage( logf, sfstate, pinglines, stats, options )
  nowday = DateTime.now
  today = nowday.to_s.split('T')  # => #<DateTime: 2022-01-01T00:00:00+00:00 --> '2022-01-01'
  # An alternative:  today = DateTime.now.strftime( "%Y-%m-%d" )  --> '2022-01-01'
  logbasename = "#{PROGNAME}-#{today}.log"
  logfilename = File.join( CONFIGDIR, logbasename )
  logfile_closed = logf ? logf.closed? : true  # test closed-state of logf only if it's non-nil
  if logfile_closed
    logf = File.open( logfilename, "a" )
    stats[:today_is] = today
  else
    # have we rolled-over midnight?  If yes, open a new file...
    nowday = DateTime.now
    newday = nowday.to_s.split('T')
    if ( newday > stats[:today_is] )
      logf.close
      logfilename = File.join( CONFIGDIR, "#{PROGNAME}-#{stats[:today_is]}.log" )
      logf = File.open( logfilename, "w" )
      stats[:today_is] = newday
    end
  end
  logf.puts "Log-File #{logfilename} opened : #{nowday}"
  stats
end  # logmessage

def pingmessage( rend, pinglines, stats, options )
  String.cls( options[:background] ) if !options[:scroll]
  ind = ( SPACE * 4 ).freeze
  errcode = rend == :red ? 'E' : 'S'
  nlines  = stats[:successrun] == 1 ? "\n\n" : ""
  $stdout.puts "#{nlines}%#{PROGNAME}-#{errcode}-ping,".bold.color(:blue) \
               + " \# #{stats[:totalpings]}".bold.color(:blue)            \
               + " -- at: #{stats[:pingtime]}".bold.color(:blue)
  if pinglines.length > 0
    $stdout.puts ind + pinglines[2].bold.color(rend) if pinglines[2] != nil  # --- 1.1.1.1 ping statistics ---
    $stdout.puts ind + pinglines[0].bold.color(rend) if pinglines[0] != nil  # PING 1.1.1.1 (1.1.1.1) 56(84) butes of data
    $stdout.puts ind + pinglines[1].color(:blue)     if pinglines[1] != nil  # 64 bytes from 1.1.1.1: icmp_seq=...
    $stdout.puts ind + pinglines[3].color(:blue)     if pinglines[3] != nil  # 1 packets transmitted, 1 received, ...
    $stdout.puts ind + pinglines[4].color(:blue)     if pinglines[4] != nil  # rtt min/avg/max/mdev = ...
  end
  # the string-editing gets complicated here, so refactor for readability...
  msg1 = ind + "-- success: (#{stats[:successrun]},#{stats[:successtotal]})"
  begin
    frag1 = "#{stats[:successstarted].elapsed.split('.')[0]}"
    msg1 += ", elapsed: #{frag1}"
  end if stats[:successstarted]
  msg2 = ind + "-- failure: (#{stats[:failurerun]},#{stats[:failuretotal]})"
  begin
    frag2 = "#{stats[:lastfailureat].to_s.split(' ')[0..1].join(' ')}"
    msg2 += ", last fail at: #{frag2}".bold.color(rend)
  end if stats[:lastfailureat] != nil
  begin
    frag3 = "#{stats[:lastfailelapsed].split('.')[0]}"
    msg2 += ", elapsed: #{frag3}".bold.color(:blue)
  end if stats[:lastfailelapsed]
  $stdout.puts msg1.bold.color(:green)
  $stdout.puts msg2.bold.color(:red)
  $stdout.puts SEPARATOR * TermChar.terminal_width
end  # pingmessage

# While in failure, make a pipe-| mark every ~1sec, and embellish
# this with a digit every 10-marks(secs), for readability.
def failmark( failurerun, horizpos )
  q, r = failurerun.divmod(10)
  fmark = r != 0 ? FAILMARK : '1234567890'[(q%10)-1]
  $stdout.print fmark
  $stdout.flush
  horizpos += 1
end  # failmark

def failmark_newline
  $stdout.print FAILINDENT
  $stdout.flush
  FAILINDENT.length - 1     # adjust for newline \n
end  # failmark_newline

# For testing & simulation of external network down/up transitions, use (Ubuntu 20.04 ...):
#    $ ip a | grep wlp
#    $ sudo ip link set  wlp4s0 down
#    $ sudo ip link set  wlp4s0 up
def pinging( nodelist, intlist, options )
  # some initializations:
  pinginterval = options[:interval]
  stats = Hash.new
  stats[:totalpings] = 0
  stats[:successtotal],   stats[:failuretotal]   = 0, 0
  stats[:successrun],     stats[:failurerun]     = 0, 0
  stats[:lastsuccessat],  stats[:lastfailureat]  = nil, nil
  stats[:successstarted], stats[:failurestarted] = nil, nil
  in_fails = false
  horizpos, twidth = 0, 0
  currentinterval = pinginterval
  # this forever-loop is terminated by user Ctrl/C --
  while true do
    nodelist.each do | pingee |
      if ( stats[:totalpings] != 0 )
        if !in_fails
          msg = "--- next: ~ #{pingee[:name]}, #{pingee[:ipaddr]} ~ in #{currentinterval} seconds...".color(:blue)
          $stdout.puts msg
        else
          msg = options[:debug] >= 1 ? "[#{currentinterval}]" : ""
          $stdout.print msg
        end
        #####################
        sleep currentinterval
        #####################
      end
      begin
        # "Give me a ping, Vasili. One ping only, please."
        #    -- CPT Marko Ramius, commander of the Soviet submarine Red October,
        #       "Hunt for Red October", Tom Clancy, 1984
        pinglines = %x{ ping -c1 -W1 #{pingee[:ipaddr]} 2> /dev/null }  # -c count -W timeout, send stderr to bit-bucket
                      .split("\n")
                      .delete_if { | ln | ln == "" }  # an array of lines, blank lines removed
      rescue RuntimeError => e
        $stderr.puts "rescued RuntimeError"
      rescue SystemCallError => e
        $stderr.puts "rescued SystemCallError"
      rescue SignalException => e
        raise  # in particular, expecting user Ctrl/C to exit from the program...
      rescue => e
        $stderr.puts "rescued something else (?)"
        e.inspect
      ensure
        pingstat = $?  # returned as #<ping status: pid 260089 exit 2> for failures...
        $stderr.puts "%#{PROGNAME}-i-pingstat, #{pingstat}" if options[:debug] >= DBGLVL1
      end
      ### pingstat = 2 if stats[:totalpings] >= 3  #  DEBUG -- to simulate a string of ping-fails
      stats[:pingtime] = Time.now
      stats[:totalpings] += 1
      if ( pingstat == SUCCESS )
        # ping success
        in_fails = false
        stats[:failurerun] = 0  # run of failures is over (hopefully)
        stats[:successrun] += 1
        if stats[:successrun] == 1 # a "brand new" run of successes?...
          stats[:successstarted] = TimeInterval.new
          if stats[:failurestarted] && !stats[:lastfailelapsed]
            stats[:lastfailelapsed] = stats[:failurestarted].elapsed
          end
        end
        stats[:failurestarted] = nil
        stats[:successtotal] += 1
        stats[:lastsuccessat] = Time.now if stats[:lastsuccessat] == nil
        ## stats[:lastfailureat] = nil <-- no, remember & display until changed
        # Once restored, return ping-interval back to default via Fibonacci-sequence in intlist...
        currentinterval = ( stats[:successrun] < intlist.length ) \
                          ? intlist[stats[:successrun]] : pinginterval
        pingmessage( :green, pinglines, stats, options )
        # Trigger log-messages on "edge"/transition from success-to-fail        ----v
        ### stats = logmessage( logf, :succeeding, pinglines, stats, options ) if stats[:successrun] == 1
      else  # ( pingstat != SUCCESS ... failing!)
        # ping failure
        $stdout.puts "  ping status: #{pingstat}".bold.color(:red) if !in_fails
        stats[:successrun] = 0  # run of successes is over (temporarily)
        stats[:failurerun] += 1
        if stats[:failurerun] == 1  # a "brand new" run of failures?...
          stats[:failurestarted] = TimeInterval.new
          stats[:lastfailureat] = Time.now
          stats[:lastfailelapsed] = nil
          stats[:successstarted] = nil
          sleep SLEEPFILTER  # filter out intermittent (<=1sec) fails...
        end
        # If link's broken, configure to ping-like-hell every second,
        # as this won't annoy anyone (hey, link's offline), and want
        # to know restoration ASAP...
        currentinterval = 1  # ping-like-hell
        case stats[:failurerun]
        when 1
          stats[:failuretotal] += 1
          pingmessage( :red, pinglines, stats, options )
          twidth = TermChar.terminal_width - INDENT
          horizpos = INDENT
          in_fails = true
          # Trigger log-messages on "edge"/transition from success-to-fail        ----v
          ### stats = logmessage( logf, :failing, pinglines, stats, options ) if stats[:failurerun] == 1
        when 2
          horizpos = failmark_newline
        else
          if horizpos < twidth
            horizpos = failmark( stats[:failurerun], horizpos )
          else
            horizpos = failmark_newline
            horizpos = failmark( stats[:failurerun], horizpos )
          end
        end  # case
      end  # if ( pingstat == SUCCESS )
    end  # nodelist.each
  end  # while
end # pinging

# === Main ===
options = { :interval   => 60,
            :background => :light,
            :scroll     => false,
            :noop       => false,
            :sudo       => "",
            :update     => false,
            :verbose    => false,
            :debug      => DBGLVL0,
            :about      => false
          }

# options.merge!( AppConfig.configuration_yaml( CONFIGFILE, options ) )

optparse = OptionParser.new { |opts|
  opts.on( "-i", "--interval=INTEGER",  Integer,
           "The interval (maximum-interval, it adjusts automatically),",
           "in seconds, between pings; default is #{options[:interval]} seconds" ) do |val|
    options[:interval] = val
  end  # -i --interval
  opts.on( "-b", "--background=SHADE", /LIGHT|DARK/i,
           "Screen background, either 'light' (default) or 'dark'" ) do |val|
    options[:background] = val.downcase.to_sym
  end  # -b --background
  opts.on( "-s", "--[no-]scroll",
           "Ping-presentation scrolls down screen, or clears-screen",
           "between each ping-report block (default)" ) do |val|
    options[:scroll] = val
  end  # -s --scroll
  # opts.on( "-«+»", "--«+»",  # "=«+»", String,
  #          "«+»" ) do |val|
  #   options[:«+»] = «+»
  # end  # -«+» --«+»
  opts.separator ""
  opts.on( "-n", "--noop", "--dryrun", "--test",
           "Dry-run (test & display, no-op) mode" ) do |val|
    options[:noop]  = true
    options[:verbose] = true  # Dry-run implies verbose...
  end  # -n --noop
  opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
    options[:verbose] = true
  end  # -v --verbose
  # --- Debug option ---
  opts.on( "-d", "--debug", "=DebugLevel", Integer,
           "Show debug information (levels: 1, 2 or 3)",
           "  1 - enables basic debugging information",
           "  2 - enables advanced debugging information",
           "  3 - enables (starts) pry-byebug debugger" ) do |val|
    options[:debug] = val.to_i
  end  # -d --debug
  # --- About option ---
  opts.on_tail( "-a", "--about", "Display program info" ) do |val|
    $stdout.puts "#{PROGID}"
    $stdout.puts "#{AUTHOR}"
    options[:about] = true
    exit true
  end  # -a --about
  # --- Set the banner & Help option ---
  opts.banner = "\n  Usage: #{PROGNAME} [options]" +
                "\n\n    Pings a list of IP-addresses, with intervening delays (dynamic)\n" +
                "    to determine link-up or link-down status, with time-of-occurence.\n" +
                "    logging, failure/success run-counts, and more.\n\n"
  opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
    $stdout.puts opts
    # $stdout.puts "«+»Additional Text«+»"
    options[:help] = true
    exit true
  end  # -? --help
}.parse!  # leave residue-args in ARGV

###############################
if options[:debug] >= DBGLVL3 #
  require 'pry'               #
  binding.pry                 #
end                           #
###############################

# Check existence of .config directory, make it if needed:
test_or_create( CONFIGDIR )

at_exit do
  $stdout.puts "  Exiting..."
  # File.close logf
  exit true
end

String.cls( options[:background] )
begin
  pinging( init_nodelist( options ), init_intlist( options[:interval] ), options )
rescue Interrupt => e
  exit true
end

# =====================================================================================

# This program is derived from an early bash shell-script, keepalive.sh, included
# below for reference:

# #!/usr/bin/env bash

# # keepalive.sh -- continuously ping a group of nodes
# #
# #  use: keepalive.sh [seconds-between-pings] [number-of-pings-per-cycle]
# #
# # Parameters:
# #   $1 = [optional] duration between watched ping commands, in seconds,
# #                   default=60 (1 minute)
# #   $2 = [optional] number of pings per, default=1
# #   $3 = [optional] if not empty string "", include MYEXPIP (my external IP-address)
# #   $4 = [optional] if not empty string "", use this list of ping-targets

# declare -i total tsuccess rsuccess tfailed rfailed xstatus

# # current 12-17-2021 - Valley Center Wireless
# MYEXTIP="76.194.191.197"

# if [ -z "$1" ]; then
#   dur=60
# else
#   dur=$1
# fi
# if [ -z "$2" ]; then
#   cnt=1
# else
#   cnt=$2
# fi
# nodelist="8.8.8.8 google.com 1.0.0.1 wga.com 8.8.4.4 ubuntu.com parsec.com 1.1.1.1 zuul.locktrack.com"  # counter.li.org
# if [ -n "$3" ]; then
#   nodelist="${MYEXTIP} ${nodelist}"
# fi
# if [ -n "$4" ]; then
#   nodelist="$@"
# fi

# xstatus=0
# tsuccess=0  # total successful pings (running total)
# rsuccess=0  # recent successful pings (without fails)
# tfailed=0   # total failed pings (running total)
# rfailed=0   # recent failed pings (without successes)
# total=0
# lastfailat=""

# clear                #...the screen
# while true ; do      #forever, or until ^C...
#   for node in $nodelist ; do
#     if [ $total -eq 0 ]; then
#       echo " ~ $node ~"
#     else
#       echo "           --    next: ~ $node ~ in $dur seconds..."
#       sleep $dur
#     fi
#     ping -c $cnt -W 1 $node
#     xstatus=$?
#     total=$((total+1))
#     if [ $xstatus = 0 ]; then
#       rfailed=0
#       rsuccess=$((rsuccess+1))
#       tsuccess=$((tsuccess+1))
#     else
#       echo "  %ping status: $xstatus"
#       rsuccess=0
#       rfailed=$((rfailed+1))
#       tfailed=$((tfailed+1))
#       lastfailat="last fail at: $(date)"
#     fi
#     echo -e "\n%keepalive -- ping#$total"
#     echo "           -- success: ($rsuccess,$tsuccess),"
#     echo "           --    fail: ($rfailed,$tfailed), $lastfailat"
#     echo "           --   total: $total"
#     echo "           --    time: $(date)"
#     done
#   done

# # exit 0
