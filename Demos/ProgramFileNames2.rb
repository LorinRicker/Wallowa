#!/usr/bin/ruby1.9.1

# ProgramFileNames2.rb
#
# Copyright © 2011-2019 Lorin Ricker <Lorin@RickerNet.us>
# Version 1.2, 08/06/2019
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

require '~/projects/ruby/lib/pquts.rb'

pquts $0, "\n2.  PROGRAM_NAME ($0)"
pquts __FILE__, "2.  __FILE__"
puts "Filenames are #{ $0 == __FILE__ ? 'equal.' : 'not equal!' }"
