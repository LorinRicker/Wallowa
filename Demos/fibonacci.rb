#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# fibonacci.rb
#
# Copyright © 2012-2019 Lorin Ricker <Lorin@RickerNet.us>
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.

# This recursive algorithm works in exponential time, with O(N**m)
# costs beginning to be noticeable on fast processors at ~ Fib(32):
def fibonacci_slow( n )
  return n if (0..1).include? n
  fibonacci_slow( n-2 ) + fibonacci_slow( n-1 )
end  # fibonacci

# But this memoized algorithm is simply linear -- it works by
# remembering all previous results for subsequent look-up,
# even though the actual algorithm remains recursive, trading
# (re)computation for memory...  Thanks to:
#  Gregory Brown, "Ruby Best Practices", "Memoization" pp. 138ff
#
# After further reflection on PickAx book, pp308-311,
# change this from a (global) instance variable to a
# proper global variable (LMR 08/06/2019):
# Initialize the series:
$fib_series = [ 0, 1 ]

def fibonacci_fast( n )
  $fib_series[n] ||= fibonacci_fast( n-2 ) + fibonacci_fast( n-1 )
end  # fibonacci_fast
alias :fib :fibonacci_fast

# Main -- test driver:
if $0 == __FILE__ then
  if ARGV[0]  # ain't nil
    require '~/projects/ruby/lib/ppstrnum'
    if ARGV[0].include?('..')
      series = Array.new
      pat = /^(?<lo>\d+)    # one or more digits
             \.{2,}         # two or more dots: '..', '...' etc
             (?<hi>\d+).*   # one or more digits
            /x
      m   = pat.match( ARGV[0] )
      if m
        m[:lo].to_i.upto( m[:hi].to_i ) { |i|
          series << fib( i )
        }
        puts "\nFib(#{m[:lo]})...Fib(#{m[:hi]}) ->\n    #{series.join( "\n    " )}"
      end
    else  # a single value
      f = fib(ARGV[0].to_i)
      puts "\nFib(#{ARGV[0]}) = #{f.thousands}"
      puts "\nor #{f.numbernames}"
    end
  else
    # The following calculated values for F(83) and F(131), both of which are
    #   also prime integers, comes from https://oeis.org/A005478 (The Online
    #   Encyclopedia of Integer Sequences), Nr. A005478, "Prime Fibonacci numbers."
    # If these print 'true', then we've got a pretty good cross-check...
    puts "Fib(83) calculation passed" if fib(83).to_s == "99194853094755497"
    puts "Fib(131) calculation passed" if fib(131).to_s == "1066340417491710595814572169"
  end
end
