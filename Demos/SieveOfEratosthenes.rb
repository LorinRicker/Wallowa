#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# SieveOfEratosthenes.rb
#
# Copyright © 2012-2019 Lorin Ricker <Lorin@RickerNet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.

# -----

require 'pp'

N = 100

# Compute prime numbers up to user-specified (or 100)
#   with the Sieve of Eratosthenes
max = ARGV[0] ? ARGV[0].to_i : N

sieve = Array.new(max + 1, true)
sieve[0] = false
sieve[1] = false

(2...max).each do |i|
  if sieve[i]
    (2 * i).step(max, i) do |j|
      sieve[j] = false
      puts " i: #{i} -- integer #{j} #{sieve[j]}" if ARGV[1]
    end
  end
end

pp sieve if ARGV[1] && max <= N

sieve.each_with_index do |prime, number|
  puts sprintf( "%4d", number ) if prime
end
exit true
