#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# countDCLlexicals.rb

# Count the number of DCL Lexical Functions,
# as cut&pasted from a VMS terminal window "HELP" display:

fwords = %w{  F$CONTEXT  F$CSID     F$CUNITS   F$CVSI     F$CVTIME   F$CVUI     F$DELTA_TIME          F$DEVICE   F$DIRECTORY           F$EDIT
  F$ELEMENT  F$ENVIRONMENT         F$EXTRACT  F$FAO      F$FID_TO_NAME         F$FILE_ATTRIBUTES     F$GETDVI   F$GETENV
  F$GETJPI   F$GETQUI   F$GETSYI   F$IDENTIFIER          F$INTEGER  F$LENGTH   F$LICENSE  F$LOCATE   F$MATCH_WILD
  F$MESSAGE  F$MODE     F$MULTIPATH           F$PARSE    F$PID      F$PRIVILEGE           F$PROCESS  F$READLINK F$SEARCH
  F$SETPRV   F$STRING   F$SYMLINK_ATTRIBUTES  F$TIME     F$TRNLNM   F$TYPE     F$UNIQUE   F$USER     F$VERIFY
}

# pp "DCL lexical functions: ", fwords
puts "Number of DCL lexical functions: #{fwords.count}"
