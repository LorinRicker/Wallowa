def acronym( inputf, outf )
  pat  = /\b[A-Z]{3,5}\b/   # just acronym-like words...
  File.open( inputf ) do | inf |
    while line = inf.gets.chomp.strip
      next if line == ''
      m = pat =~ line   # or:  m = pat.match( line )
      if m
        puts "Acronym '#{m[0]}' in: ", line  # m[0] is same as $&
      end
    end  # while
  end
rescue Errno::ENOENT => e
  STDERR.puts "%#{PROGNAME}-e-fnf, error opening input file '#{inputf}'"
  exit false
end
