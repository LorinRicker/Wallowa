#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# dclappend.rb
#
# Copyright © 2019 Lorin Ricker <Lorin@RickerNet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.

# Utility to imitate VMS DCL's APPEND file command.
#
#    This may ultimately be added as the APPEND command in dcl.rb...
#

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v1.5 (09/07/2019)"
  AUTHOR = "Lorin Ricker, Valley Center, California, USA"

SPC = ' '.freeze
USAGE = "\n  Usage: #{PROGNAME} [options] [ textfile... | -- ] apptextfile".freeze

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for binding.pry &/or pry-{byebug|nav} #
             ######################################################

require 'optparse'

# -----
def copy_getabyte( args, outfspec, outf, options )
  args.each do | infspec |
    File.open( infspec, 'rb' ) do | inf |
    $stderr.puts "%#{PROGNAME}-i-bytes, appending '#{infspec}' to '#{outfspec}'" if options[:verbose]
      while !inf.eof?
        outf.putc inf.getbyte   # IO safe: 1 byte at a time...
      end  # while
    end
  end
end  # copy_getabyte

def copy_lineatime( args, outfspec, outf, options )
  ars.each do | infspec |
    File.open( infspec, 'r' ) do | inf |
      $stderr.puts "%#{PROGNAME}-i-lines, appending '#{infspec}' to '#{outfspec}'" if options[:verbose]
      while !inf.eof?
        outf.put inf.get   # 1 line at a time
      end  # while
    end
  end
end  # copy_lineatime

def copy_stdin( outfspec, outf, options )
  $stderr.puts "%#{PROGNAME}-i-stdin, appending 'stdin' to '#{outfspec}'" if options[:verbose]
  $stdout.print options[:prompt] if options[:prompt]
  while !$stdin.eof?
    outf.puts  $stdin.gets
    $stdout.print options[:prompt] if options[:prompt]
  end  # while
end  # copy_stdin

def copy_zerocopy( args, outfspec, outf, options )
  require 'io/splice'
  # Use a pipe as a ring-buffer in kernel space;
  # pipes can store up to IO::Splice::PIP_CAPA bytes.
  pipe = IO.pipe
  piperfd, pipewfd = pipe.map { |io| io.fileno }
  outffd = outf.fileno

  args.each do | infspec |
    $stderr.puts "%#{PROGNAME}-i-zerocopy, appending '#{infspec}' to '#{outfspec}'" if options[:verbose]
    File.open( infspec, 'rb' ) do | inf |
      inffd = inf.fileno
      while true
        nbytesread = begin
          # Each gulp pulls as many bytes as possible into the pipe...
          IO.splice( inffd, nil, pipewfd, nil, IO::Splice::PIPE_CAPA, 0 )
        rescue EOFError
          # fully consumed this infspec file, just continue...
          break
        end
        # Each gulp gets xferred (in the kernel!) to the destination;
        # this copied data never touches (enters) userspace:
        nbyteswritten = begin
          IO.splice( piperfd, nil, outffd, nil, nbytesread, 0 )
        rescue SystemCallError => e
          # Expect "no file access or protection violation" issues here
          $stderr.puts "%#{PROGNAME}-e-fileaccess, cannot append '#{outfspec}'"
          $stderr.puts "%#{PROGNAME}-e-rescued, #{e}", e.to_s
          exit false
        end
        $stderr.puts "%{PROGNAME}-i-bytes, read: #{nbytesread} -> written: #{nbyteswritten}" if options[:verbose]
      end  # while
      # Voila!  infspec has been appended to outfspec in kernel space
    end
  end
end  # copy_zerocopy
# -----

options = { :prompt   => nil,
            :timer    => nil,
            :copy     => :zerocopy,
            :verbose  => false,
            :debug    => DBGLVL0,
            :about    => false
          }

optparse = OptionParser.new { |opts|
  opts.on( "-p", "--prompt[=PSTRING]", "Prompt text (optional) for each data line;",
           "if this option is used without providing",
           "the PSTRING prompt-text, the default prompt",
           "text is 'append: '; terminate input with Ctrl/D" ) do |val|
    val += SPC if !val.nil? && val[val.length-1] != SPC  # not nil? add a trailing space if needed...
    options[:prompt] = val || 'append: '
  end  # -p --prompt
  opts.on( "-c", "--copy=METHOD", /z.*|l.*|b.*/i,
           "Copy-method to use (Zerocopy, Line, Byte):",
           "Zerocopy (default, fastest),",
           "Line-at-a-time, Byte-at-a-time" ) do |val|
    options[:copy] = case val[0].downcase.to_sym
                     when :z then :zerocopy
                     when :l then :lineatime
                     when :b then :getabyte
                     end
  end  # -c --copy
  opts.on( "-t", "--timer", "Time this append operation, report duration" ) do |val|
    options[:timer] = val
  end  # -t --timer
  # --- Verbose option ---
  opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
    options[:verbose] = true
  end  # -v --verbose
  # --- Debug option ---
  opts.on( "-d", "--debug", "=DebugLevel", Integer,
           "Show debug information (levels: 1, 2 or 3)",
           "  1 - enables basic debugging information",
           "  2 - enables advanced debugging information",
           "  3 - enables (starts) pry-byebug debugger" ) do |val|
    options[:debug]   = val.to_i
    options[:verbose] = true
  end  # -d --debug
  # --- About option ---
  opts.on_tail( "-a", "--about", "Display program info" ) do |val|
    require_relative 'lib/AboutProgram'
    options[:about] = about_program( PROGID, AUTHOR, true )
  end  # -a --about
  # --- Set the banner & Help option ---
  opts.banner = USAGE +
                "\n\n    Appends file(s) (&/or stdin) to an appended/output file." +
                "\n    If the append/output file does not exist, it is created." +
                "\n    If it already exists, input file(s) are concatenated to it." +
                "\n    If no input file(s) are given, then text lines from Standard"  +
                "\n    Input (stdin) are appended; use Ctrl/D to finish.\n\n"
  opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
    $stdout.puts opts
    options[:help] = true
    exit true
  end  # -? --help
}.parse!  # leave residue-args in ARGV

###############################
if options[:debug] >= DBGLVL3 #
  require 'pry'               #
  binding.pry                 #
end                           #
###############################

pp options if options[:debug] >= DBGLVL1
require_relative 'lib/TimeInterval' if options[:timer]

if !ARGV[0]
  $stderr.puts USAGE
  exit false
end

starttime = TimeInterval.new( opts = options, start_ts = Time.now ) if options[:timer]

outfspec = ARGV.pop  # last element is the outfile-to-append-to... ARGV's now one-shorter
  File.open( outfspec, 'a' ) do | outf |
    if ARGV.length > 0
      case options[:copy]
      when :getabyte
        copy_getabyte( ARGV, outfspec, outf, options )
      when :lineatime
        copy_lineatime( ARGV, outfspec, outf, options )
      when :zerocopy
        copy_zerocopy( ARGV, outfspec, outf, options )
      end  # case
    else  # working on standard-input ($stdin) --
      copy_stdin( outfspec, outf, options )
    end
  end

$stderr.puts "%#{PROGNAME}-i-elapsed, this append took #{starttime.elapsed}" if options[:timer]
exit true
