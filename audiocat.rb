#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# audiocat.rb
#
# Copyright © 2012-2025 Lorin Ricker <Lorin@RickerNet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.

# ===========
#  Problem: When a multi-movement music composition (sonata, etc) is ripped from
#           a CD (as ogg or mp3 files), the individual movements rip to separate,
#           individual files.  If these are simply copied over to an audio player
#           (iPod, SanDisk Sansa Clip, smartphone, etc), then that particular
#           composition is likely to be played back with movements either out-of-
#           order, or else separated by other pieces.  This happens regardless
#           of the play mode (shuffle, most-recent, playlist, genre, etc).

# Solution: Copy-concatenate the individual (separate track) movement files into
#           a single multi-movement composition file.

# Specifics --
#  Ogg Vorbis - Data elements are highly structured, file-aware, and cannot
#  & Flac       simply be copy-concatenated -- well, yes they can, but c-c'd
#               files likely will not play back correctly on most players
#               (VLC does work), as most players will terminate playback when
#               they see the last-frame special bit set (as it is for each
#               individually ripped file).  Instead, the multiple files must
#               be structurally copied using ffmpeg (formerly used oggCat).
#
#  MP3        - Data elements are "audio frames" (no file abstraction),
#               so multiple MP3 files can be copy-concatenated directly
#               (similar to $ cat m1.mp3 m2.mp3 m3.mp3 >work.mp3)...
#               the method copycat implements this directly.

# ===========
# Command Line --
#   $ audiocat [options] outfile infile1 infile2 [...]
#
#   Default input/output file type is .ogg (can omit the ".ogg" file extension);
#   use --type flac  (or specify file extensions as ".flac") to copy-cat Flac audios,
#   or --type mp3 (or specify file extensions as ".mp3") to copy-cat MP3s.
#
#   Patterned input files can be specified as a range:
#     $ audiocat "Sonata Op 31" [options] track{1..4}
#   expands into:
#     $ audiocat [options] "Sonata Op 31" track1 track2 track3 track4

# ===========
# Reference & research --
#   http://www.vsdsp-forum.com/phpbb/viewtopic.php?f=8&t=347
#
# Install these packages --
#   vorbis-tools, oggvideotools, ruby-ogginfo, tagtool
#
# Analysis --
#   $ hexdump -Cn 1024 ogg-file
#
# See also these Ogg Vorbis tools:
#   ffmpeg        -- stronger tool for Flac, Ogg Vorbis, and other audio files
#   oggCat        -- correctly copy/concatenates Ogg Vorbis (audio) files
#                    (does not simply copy-chain them...)
#   ogginfo       -- parses Ogg Vorbis files with extensive validity checking
#   vorbiscomment -- List & edit Ogg Vorbis comments (single file)
#   vorbistagedit -- batch editing of Ogg Vorbis comments with an editor (nano)
#   tagtool       -- (GUI) editing of Ogg Vorbis comments (single/multi-files)

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v4.0 (03/08/2025)"
  AUTHOR = "Lorin Ricker, Valley Center, California, USA"

   CONFIGDIR = File.join( ENV['HOME'], ".config", PROGNAME )
  CONFIGFILE = File.join( CONFIGDIR, "#{PROGNAME}.yaml.rc" )

   AUDIOTOOL = 'ffmpeg'   # formerly 'oggCat'
    TEMPFILE = './AUDIOCAT_TMP.TXT'

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for binding.pry &/or pry-{byebug|nav} #
             ######################################################
# ==========

require 'optparse'
require 'pp'
require 'fileutils'
require_relative 'lib/appconfig'
require_relative 'lib/filemagic'
require_relative 'lib/ANSIseq'
require_relative 'lib/FileParse'
require_relative 'lib/FileEnhancements'
require_relative 'lib/TimeInterval'

# ==========

def config_save( opt )
  # opt is a local copy of options, so we can patch a few
  # values without disrupting the original/global hash --
  opt[:about]     = false
  opt[:debug]     = DBGLVL0
  opt[:update]    = false
  opt[:verbose]   = false
  AppConfig.configuration_yaml( CONFIGFILE, opt, true )  # force the save/update
end  # config_save

def copycat( outfile, infiles, options )
  # Copy-catenate infiles to outfile
  ifl, inf = "", ""
  ofl = File.basename outfile
  begin
    File.open( outfile, mode: "ab" ) do | outf |
      infiles.each_with_index do | infile, idx |
        ifl = File.basename infile
        # $stderr.puts "%#{PROGNAME}-I-COPYCAT, \"#{ifl}\" >> \"#{ofl}\"" if options[:verbose]
        # Inhale and exhale the whole thing...
        unless options[:dryrun]
          inf = File.open( infile, mode: "rb" )
          IO.copy_stream( inf, outf )
          inf.close
        end  # unless options[:dryrun]
      end  # infiles.each_with_index
    end  # File.open( outfile, ... )
  rescue StandardError => e
    $stderr.puts "%#{PROGNAME}-E-COPYCAT, error in copying: '#{ifl}'"
    pp e
    print e.backtrace.join( "\n" )     # Catch-all, display the unexpected...
    exit false                         # stop in our tracks
  end
end  # copycat

def renfiles( outfile, infile, options )
  $stderr.puts "%#{PROGNAME}-I-MV, rename \"#{infile}\" \"#{outfile}\"" if options[:verbose]
  FileUtils.mv( infile, outfile, **{ :force => true } ) unless options[:dryrun]
end  # renfiles

def mk_ffmpeg_infiles( infiles, aclistfile = TEMPFILE )
  # Create input-file-list for ffmpeg:
  # file 'infile01'
  # file 'infile02'
  # ...
  File.open( aclistfile, mode: "w" ) do | flistf |
    infiles.each do | inf |
      flistf.puts( "file '#{inf}'")
    end
  end
end  # mk_ffmpeg_infiles

def list_ffmpeg_infiles( aclistfile, options )
  # (diagnostic) Open and list all lines in current input-list file
  if options[:verbose] || options[:debug] >= DBGLVL1
    $stdout.puts "----- Input-list file: '#{aclistfile}': -----"
    File.open( aclistfile, mode: "r" ).each_line { |inf| $stdout.puts inf }
    $stdout.puts "-----"
  end
end  # list_ffmpeg_infiles

def delfiles( infiles, options )
  $stderr.puts "%#{PROGNAME}-I-RM, delete \"#{TEMPFILE}\"" if options[:verbose]
  FileUtils.rm TEMPFILE unless options[:dryrun]
  ifl = ""
  begin
    # Clean-up... remove (with permission) all of the infiles
    infiles.each do | infile |
      ifl = File.basename infile
      $stderr.puts "%#{PROGNAME}-I-RM, delete \"#{ifl}\"" if options[:verbose]
      FileUtils.rm [ infile ] unless options[:dryrun]
    end  # infiles.each
  rescue StandardError => e
    $stderr.puts "%#{PROGNAME}-E-DELFILES, error in deleting: '#{ifl}'"
    pp e
    print e.backtrace.join( "\n" )     # Catch-all, display the unexpected...
    exit true                          # stop in our tracks
  end
end  # delfiles

def concatenate_byterate( outfile, elapsedtime, displaytime )
  outfsize = File.lstat( outfile ).size
  byterate = File.size_human_readable( ( outfsize / elapsedtime ) )
  $stdout.puts "%#{PROGNAME}-I-STAT, concatenated at #{byterate}bytes/sec, elapsed #{displaytime}"
end  # concatenate_byterate

def update_todays_logfile( outfile, options )
  # Open logfile-for-today in append mode, which creates the file
  # if it doesn't yet exist, or opens it for appends if it does exist.
  # Add comment text to logfile if options[:comment]
  # Add entire outfile (ARGV[0]) text to logfile
  logfile = "./" + PROGNAME + "-" + Time.new.to_s.split(" ")[0] + ".lis"
  outtext = File.basename( outfile, ".*" )
    # e.g., "./audiocat-2019-08-04.lis"
  File.open( logfile, "a" ) do | logf |
    logf.puts "--- #{options[:comment]}" if options[:comment]
    logf.puts "    #{outtext}"
  end
  # copy (-f : replace) logfile to options[:syncdir] if options[:syncdir]
  FileUtils.cp( logfile, options[:syncdir] ) if options[:syncdir]
end  # update_todays_logfile

# ==========

options = Hash.new
options.merge!( AppConfig.configuration_yaml( CONFIGFILE, options ) )
ARGV[0] = '--help' if ARGV.size == 0  # force help if naked command-line

optparse = OptionParser.new { |opts|
  opts.on( "-t", "--type", "=AUDIO", /mp3|ogg|wav/i,
           "Audio-type of files (ogg (d), mp3, wav)" ) do |val|
    options[:type] = val.downcase || "ogg"
  end  # -t --type
  opts.on( "-n", "--dryrun", "Dry run: don't actually copy or delete files" ) do |val|
    options[:dryrun] = true
  end  # -n --dryrun
  opts.on( "-r", "--remove", "Remove (delete, default is keep) input files" ) do |val|
    options[:remove] = true
  end  # -r --remove
  opts.on( "-c", "--comment", "Comment text for logfile" ) do |val|
    options[:comment] = val
  end  # -c --comment
  opts.on( "-s", "--syncdir", "Sync-directory for copy of logfile" ) do |val|
    options[:syncdir] = val
  end  # -s --syncdir
  opts.on( "-u", "--update", "--save",
           "Update (save) the configuration file; a configuration",
           "file is automatically created if it doesn't exist:",
           "#{CONFIGFILE}" ) do |val|
    options[:update] = val
  end  # -u --update
  # --- Verbose option ---
  opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
    options[:verbose] = true
  end  # -v --verbose
  # --- Debug options ---
  opts.on( "-D", "--debug", "=DebugLevel", Integer,
           "Show debug information (levels: 1, 2 or 3)",
           "  1 - enable basic debug information output",
           "  2 - enable advanced debug information output",
           "  3 - enable (starts) 'Ruby debug' gem debugger" ) do |val|
    options[:debug] = val.to_i
  end  # -D --debug
  opts.on( "-P", "--pry", "Debug with pry (rather than 'Ruby debug' gem)") do |val|
    options[:prydebug] = true
    options[:debug] = DBGLVL3
  end  # -P --pry
  # --- About option ---
  opts.on_tail( "-a", "--about", "Display program info" ) do |val|
    require_relative 'lib/AboutProgram'
    options[:about] = about_program( PROGID, AUTHOR, true )
  end  # -a --about
  # --- Set the banner & Help option ---
  opts.banner = "\n  Usage: #{PROGNAME} [options] output-audio-file input-audio-file(s)" +
                "\n\n   Note: The output file is first, then two or more input files.\n\n"
  opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
    $stdout.puts opts
    options[:help] = true
    exit true
  end  # -? --help
}.parse!  # leave residue-args in ARGV

#################################
# Revised version of this block #
# now permits use of either     #
# Ruby Debug gem or Pry gem     #
#################################
if options[:debug] >= DBGLVL3   #
  if options[:prydebug] # -P    #
    require 'pry'               #
    binding.pry                 #
  else                          #
    require 'debug'     # -Dn   #
    binding.break               #
  end                           #
end                             #
#################################

# Propagate a couple of implications --
# (which should *not* be saved in the CONFIGFILE):
options[:remove] = false if options[:dryrun]      # dryrun always implies keep...
options[:debug]  = DBGLVL1 if options[:dryrun]    # ...and also debug...
options[:verbose] ||= options[:debug] > DBGLVL0   # ...and debug implies verbose

puts "%#{PROGNAME}-I-FTYPE, audio filetype is '#{options[:type]}'" if options[:debug] > DBGLVL0

# Update the config-file, at user's request:
config_save( options ) if options[:update]

# Working with this file-type (extension):
fext = ".#{options[:type]}"

# Output file is the _first_ filespec in the ARGV list, shifting it out:
outfile = File.expand_path( File.default_extension( ARGV.shift, fext ) )

# Expand shorthands like two- "{1..5}" and three-dot "{1...5}" (either syntax is
# allowed), surrounded by "{}", into multiple files, e.g.:
#   "Track1..5"     --> "Track1", "Track2", "Track3", "Track4", "Track5"
#   "Track 12...14" --> "Track 12", "Track 13", "Track 14",... "Track 17"
pat = Regexp.new( /^(.*?)    # any prefix (lazy)  m[1]
                  \{         #
                  ([0-9]+)   # 1 or more digits   m[2]
                  (\.{2,3})  # ".." or "..."      m[3]
                  ([0-9]+)   # 1 or more digits   m[4]
                  \}         #
                  (.*)       # any suffix         m[5]
                  /ix )      # ..."Lazy" prefix gives _all_ of the first
                             #    digits to the first integer value
infiles = []
argfiles = []

ARGV.each do | f |    # Each remaining file in ARGV is an input filespec...
  $stderr.puts "%#{PROGNAME}-I-ARGV, '#{f}'" if options[:debug] > DBGLVL0
  m = pat.match( f )
  if m   # Matched a pattern file-range "lo..hi",
         # so roll thru the file-range here...
    cur  = m[2]
    zcur = sprintf( '%03d', m[2].to_i )  # Conversion here can be tricky, sometimes
    zhi  = sprintf( '%03d', m[4].to_i )  # m[2] or m[4] gets misinterpreted as octal
    while zcur <= zhi  # hi of "lo..hi"
      inf = m[1] + cur + m[5]
      inf = File.default_extension( m[1] + cur + m[5], fext )
      Dir.glob( inf ).each { |x| argfiles << x }
      cur  = cur.succ   # succ does just what we want!...
      zcur = zcur.succ
    end  # while zcur <= zhi
  else
    g = File.extname( f ) != "" ? f : "#{f}#{fext}"
    Dir.glob( g ).each { |x| argfiles << x }
  end  # if m
  # Careful -- globs with 2 or more wildcards (e.g., "*-{01..32}-*")
  #            can yield duplicate filespecs, so impose uniqueness:
end  # ARGV.each

argfiles.uniq.each { |x| infiles << x }
infcount = infiles.size
pp infiles if options[:debug] > DBGLVL0

fnflag = badflag = false
infiles.each do | inf |   # Validate each input file...
  binf = File.basename(inf)
  if File.exist?( inf )
    $stderr.puts "%#{PROGNAME}-I-INFILE, '#{binf}'" if options[:verbose]
    if ! inf.verify_magicnumber( options[:type] )
      $stderr.puts "%#{PROGNAME}-E-BADMAGIC, wrong file signature: #{binf}"
      badflag = true
    end  # if
    if inf.index( "'" )
      $stderr.puts "%#{PROGNAME}-E-BADCHAR, apostrophe == single-quote,"
      $stderr.puts " -W-RENAME, rename file to remove single-quote character(s)"
      badflag = true
    end  # if inf.index()
  else
    $stderr.puts "%#{PROGNAME}-E-FNF, file not found: #{binf}"
    fnflag = true
  end  # if File.exist?( inf )
end  # infiles.each
exit true if fnflag || badflag

$stderr.puts "%#{PROGNAME}-I-OUTFILE, '#{File.basename(outfile)}'" if options[:verbose]

if infcount == 1
  # This case devolves to just a file rename, no infiles deletion needed...
  renfiles( outfile, infiles[0], options )
  filez = "file"  # ...just one
  update_todays_logfile( outfile, options )
else
  # Copy-concatenate all infiles to the single outfile, & conditionally delete infiles
  case options[:type].to_sym
  when :ogg, :flac
    # See $ man ffmpeg --
    #   1. ffmpeg has many options, but concatenation is pretty simple.
    #   2. Input audio files are provided as an input-list file (see mk_ffmpeg_infiles()...)
    #   3. -map 0:a selects stream-0 as input-"a"
    #   3. ffmpeg barfs out noise messages, so optionally shunt the noise to /dev/null/ ...
    mk_ffmpeg_infiles( infiles, TEMPFILE )  # (re)creates AUDIOCAT_TMP input-list file
    list_ffmpeg_infiles( TEMPFILE, options )
    ffunc   = "-f concat"
    infunc  = "-i #{TEMPFILE}"
    mapping = "-map 0:a"  # select stream-0 as input-"a"
    copying = "-c:a copy"
    noise   = options[:debug] <= DBGLVL1 ? "2>/dev/null " : ""
    cmd = "#{AUDIOTOOL} #{ffunc} #{infunc} #{mapping} #{copying} '#{outfile}' #{noise}"
    $stderr.puts "%#{PROGNAME}-I-ECHO, $ #{cmd}" if options[:debug] > DBGLVL0
    $stdout.puts "%#{PROGNAME}-I-WORKING, be patient - #{AUDIOTOOL} converting..."
    cattimer = TimeInterval.new
    %x{ #{cmd} } unless options[:dryrun]
    stat = !options[:dryrun] ? $?.exitstatus : 0
    if stat == 0 && !options[:dryrun]
      elapsedtime = cattimer.stoptime
      # TimeInterval's @accumulated_seconds is the elapsed time interval:
      concatenate_byterate( outfile, cattimer.elapsed_seconds, cattimer.format_interval.strip )
      delfiles( infiles, options ) if options[:remove]
      update_todays_logfile( outfile, options )
    else
      $stderr.puts "%#{PROGNAME}-E-STAT, #{AUDIOTOOL} exit: #{stat}"
      $stderr.puts "-W-INSTALLED, is #{AUDIOTOOLS} installed?" if stat == 127
      exit stat
    end  # if stat == 0
  when :mp3
    if options[:debug] > DBGLVL0
      $stderr.puts "%#{PROGNAME}-I-CALL, copycat()..."
    else
      copycat( outfile, infiles, options )
      delfiles( infiles, options ) if options[:remove]
      update_todays_logfile( outfile, options )
    end  # if options[:debug] > DBGLVL0
  else
    $stderr.puts "%#{PROGNAME}-E-BADFILE, unsupported file type #{options[:type]}"
    exit false
  end  # case options[:type]
  filez = "files"  # ...several
end  # if infcount == 1

# Summary...
$stderr.puts "%#{PROGNAME}-I-CONCATENATED, #{infcount} #{filez} >> \"#{File.basename outfile}\"" if options[:verbose]
exit true
