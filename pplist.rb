#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# pplist.rb
#
# Copyright © 2019 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v1.0 (02/14/2019)"
  AUTHOR = "Lorin Ricker, Valley Center, California, USA"

# Only these chars _V_ are elegible for guess_separator detection:
 BSLASH = '\\'
  COLON = ':'     # *
 DOLLAR = '$'
  COMMA = ','     # *
 PERIOD = '.'     # *
  SEMI  = ';'     # *
  SLASH = '/'
  SPACE = ' '     # *
VERTBAR = '|'

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for binding.pry &/or pry-{byebug|nav} #
             ######################################################
# -----

require 'optparse'    # standard library
require_relative 'lib/StringCases'

# ============

def guess_separator( list, sepset )
  # The ratio of the total length of the list to the number of separator-chars
  # counted in that list is just a dumb little heuristic to detect one of several
  # possible separator characters in the list.  Works okay for $PATH, $CDPATH,
  # $FREQGITREPOLIST and related, but may break down for more general use cases...
  sepset.each do | sep |
    len   = list.length.to_f
    ratio = len / ( list.count( sep ) + 1 )
    return sep if ratio.between?( 1.5, len -1 )
  end
  # else, having searched thru the whole set of separators...
  return nil  # no good separator found...
end # guess_separator

def report( list )
  list.each { | elem | STDOUT.puts "  #{elem}" }
end # report

# === Main ===
options = { :separator  => nil,
            :verbose => false,
            :debug   => DBGLVL0,
            :about   => false
          }

optparse = OptionParser.new { |opts|
  opts.on( "-s", "--separator=CHAR", "--sep", String,
           "Separator character, default is a space (blank ' ').  Common separator",
           "characters are SPACE (' '), COLON (':'), COMMA (','), PERIOD or DOT ('.')",
           "and SEMICOLON (';').  When you use this option, you must provide a single-",
           "character separator value, usually enclosed in quotes, like --sep=';' ...\n\n",
           "For common environment variables like PATH or CDPATH (colon-separated)",
           "or FREQGITREPOLIST (and friends, space-separated), pplist can often",
           "auto-determine the separator, so this option is optional for common",
           "use cases.  Specify a separator character for the unconventional or",
           "uncommon cases, or when things don't display as you want.\n\n" ) do |val|
    options[:separator] = val[0]
  end  # -«+» --«+»
  # --- Verbose option ---
  opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
    options[:verbose] = true
  end  # -v --verbose
  # --- Debug option ---
  opts.on( "-d", "--debug", "=DebugLevel", Integer,
           "Show debug information (levels: 1, 2 or 3)",
           "  1 - enables basic debugging information",
           "  2 - enables advanced debugging information",
           "  3 - enables (starts) pry-byebug debugger" ) do |val|
    options[:debug] = val.to_i
  end  # -d --debug
  # --- About option ---
  opts.on_tail( "-a", "--about", "Display program info" ) do |val|
    $stdout.puts "#{PROGID}"
    $stdout.puts "#{AUTHOR}"
    options[:about] = true
    exit true
  end  # -a --about
  # --- Set the banner & Help option ---
  opts.banner = "\n  Usage: #{PROGNAME} [options] ENV-VAR-NAME" +
                "\n     or: #{PROGNAME} [options] $ENV-VAR-NAME" +
                "\n\n   where ENV-VAR-NAME is the name of a list-type environment variable to pretty-print display.\n\n"
  opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
    $stdout.puts opts
    # $stdout.puts "«+»Additional Text«+»"
    options[:help] = true
    exit true
  end  # -? --help
}.parse!  # leave residue-args in ARGV

###############################
if options[:debug] >= DBGLVL3 #
  require 'pry'               #
  binding.pry                 #
end                           #
###############################

# if !options[:separator]
#   options[:separator] = COLON
# end

if ARGV.count > 1
  # For certain cases, like $ pplist $FREQGITREPOLIST, shell will have already
  # done the word splits, based on separating spaces (blanks), into components
  # of ARGV, so just report 'em:
  report( ARGV )
elsif ARGV.count == 1
    # Was argument eval'd/expanded on shell command line as $ENVAR?
  if ARGV[0].isallUPCASE? && ARGV[0][0] != DOLLAR
    # If not, expand its value here...
    list = ENV["#{ARGV[0]}"]
  else
    # If it was, just copy the shell-expanded value...
    list = ARGV[0]
  end
  options[:separator] ||= guess_separator( list, [COLON,COMMA,PERIOD,SEMI,SPACE] )
  if options[:separator]
    STDOUT.puts "separator = '#{options[:separator]}'" if options[:verbose]
    report( list.split( options[:separator]) )
  else
    prfx = "%#{PROGNAME}-e-no_valid_sep,"
    STDERR.puts "#{prfx} no valid separator determined;"
    STDERR.puts "#{SPACE*prfx.length} use an explicit --separator=CHAR on command line"
  end
else
  prfx = "%#{PROGNAME}-e-no_arg,"
  STDERR.puts "#{prfx} missing argument;"
  STDERR.puts "#{SPACE*prfx.length} provide a list environment variable name"
end

exit true
