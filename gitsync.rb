#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# gitsync.rb
#
# Copyright © 2018 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

# Based on an earlier bash script: gitsync.sh
#
# Tests the currency of one or more local .git repositories against
# the corresponding remote (GitLab, formerly GitHub) repo.
#

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v1.2 (03/10/2019)"
  AUTHOR = "Lorin Ricker, Valley Center, California, USA"

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for binding.pry &/or pry-{byebug|nav} #
             ######################################################

GRL = "FREQGITREPOLIST"
NL  = "\n"
# -----

require 'optparse'  # standard library
require_relative 'lib/ANSIseq'

def check_git_sync( repo, options, repocount )
  result = ""
  STDOUT.puts "=====\n  #{repocount}: #{repo.bold.color(:red)}"
  if ! options[:verbose]  # do it...
    result += NL + %x{ cd #{repo} && git pull origin master } if options[:check]
    result += NL + %x{ cd #{repo} && git remote -v show }     if options[:show]
    result += NL + %x{ cd #{repo} && git status }             if options[:status]
  else  # just echo what would be done...
    result += NL + "%x{ cd #{repo} && git pull origin master }" if options[:check]
    result += NL + "%x{ cd #{repo} && git remote -v show }"     if options[:show]
    result += NL + "%x{ cd #{repo} && git status }"             if options[:status]
  end
  return result
end # check_git_sync

# === Main ===
options = { :verbose => false,
            :debug   => DBGLVL0,
            :about   => false,
            :check   => true,
            :status  => true,
            :show    => false
          }

optparse = OptionParser.new { |opts|
  # --- gitsync options ---
  opts.on( "-c", "--[no-]check", "--[no-]pull",
           "Check remote-vs-local git syncrony (default)" ) do |val|
    options[:check] = val
  end  # -c --check
  opts.on( "-t", "--[no-]status", "Show local git commit status (default)" ) do |val|
    options[:status] = val
  end  # -t --status
  opts.on( "-s", "--[no-]show", "Show remote git repo" ) do |val|
    options[:show] = val
  end  # -s --show
  opts.separator "    -----"
  # --- Verbose option ---
  opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
    options[:verbose] = true
  end  # -v --verbose
  # --- Debug option ---
  opts.on( "-d", "--debug", "=DebugLevel", Integer,
           "Show debug information (levels: 1, 2 or 3)",
           "  1 - enables basic debugging information",
           "  2 - enables advanced debugging information",
           "  3 - enables (starts) pry-byebug debugger" ) do |val|
    options[:debug] = val.to_i
  end  # -d --debug
  # --- About option ---
  opts.on_tail( "-a", "--about", "Display program info" ) do |val|
    $stdout.puts "#{PROGID}"
    $stdout.puts "#{AUTHOR}"
    options[:about] = true
    exit true
  end  # -a --about
  # --- Set the banner & Help option ---
  opts.banner = "\n  Usage: #{PROGNAME} [options] [repo1] [repo2]..." +
                "\n     or: #{PROGNAME} [options] $*GITREPOLIST" +
                "\n\n    where repo1 (etc) is a list of repository directories" +
                "\n    or 'logical name(s)' for repo-directories." +
                "\n\n    If no argument is given, the user's env-var #{GRL} is checked;" +
                "\n    current: FREQGITREPOLIST, DOCGITREPOLIST and ALLGITREPOLIST.\n\n"
  opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
    $stdout.puts opts
    # $stdout.puts "«+»Additional Text«+»"
    options[:help] = true
    exit true
  end  # -? --help
}.parse!  # leave residue-args in ARGV

pp options if options[:verbose]

###############################
if options[:debug] >= DBGLVL3 #
  require 'pry'               #
  binding.pry                 #
end                           #
###############################

repocount = 1

if ARGV.count > 0
  ARGV.each do | repo |
    STDOUT.puts check_git_sync( repo, options, repocount )
    repocount += 1
  end
else
  repos = ENV["#{GRL}"].split
  if repos.count > 0
    repos.each do | repo |
      STDOUT.puts check_git_sync( repo, options, repocount )
      repocount += 1
    end
  else
    STDERR.puts "%#{PROGNAME}-e-norepos, no repos to sync (check env-var #{GRL})"
  end
end

exit true
