#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# numberlines.rb
#
# Copyright © 2019 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

# This command-utility is pipe-able as a stage-1 component only, outputs to StdOut;
# it does require an actual text-file (not StdIn) -- reasonable, since the file is
# intended to be line-numbered.

# A tool to number lines from 1-to-N, with optional field-size and
# gutter character(s) specifications
#

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v1.5 (05/24/2019)"
  AUTHOR = "Lorin Ricker, Valley Center, California, USA"

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for binding.pry &/or pry-{byebug|nav} #
             ######################################################

STDINFD  = 0
STDOUTFD = 1

require 'optparse'
require 'fileutils'

# =====

def fnf_error_exit( e, infspec )
  STDERR.puts "%#{PROGNAME}-e-fnf, error opening file '#{infspec}'"
  exit false
end # fnf_error_exit

def number_lines( inputf, options )
  File.open( inputf ? inputf : STDINFD, "r" ) do | inf |
    outln = ""
    lnnr  = 1
    fldwidth = options[:fldwidth][0].upcase == 'C' ?        # C[OUNTLINES]?
               %x{ wc -l #{inputf} }.to_i.to_s.size + 1 :   # => ' 88 filename' -> 88 -> '88' -> 2+1
               options[:fldwidth].to_i                      # else use what's given or default
    while ln = inf.gets
      ln = ln.chomp
      # Line-number digits always followed by 1 space ' '...
      case options[:format]
      when :text
        outln = sprintf( '%1$*2$s %3$s%4$s',
                         lnnr.to_s, fldwidth, options[:gutter], ln )
      when :rtf
        if lnnr > 1
          outln = sprintf( '%1$*2$s %3$s%4$s',
                           lnnr.to_s, fldwidth, options[:gutter], ln )
        else  # when lnnr == 1, don't prefix, as this destroys the RTF-doc headerline
          outln = ln
          msg = "%#{PROGNAME}-i-line1, "
          firstnum = sprintf( '%1$*2$s %3$s',
                              '1', fldwidth, options[:gutter] )
          STDERR.puts "#{msg}first RTF-declaration line un-numbered;"
          STDERR.puts " " * msg.length + "manually add '#{firstnum}' in final edit..."
        end
      end  # case
      STDOUT.puts outln
      lnnr += 1
    end  # while
  end
rescue Errno::ENOENT => e
  fnf_error_exit( e, inputf )
end  # number_lines

def unnumber_lines( inputf, options )
  File.open( inputf ? inputf : STDINFD, "r" ) do | inf |
    outln = ""
    # Gutter-chars likely include regexp metachars like '|', etc...
    esc_gutter = Regexp.escape( options[:gutter] )
    case options[:unnumber]
    when :both
      pat = /^\s*\d+ #{esc_gutter}/
    when :numbers
      pat = /^\s*\d+ /  # plus that one default gutter-space (only)!...
    when :gutter
      pat = /^(\s*\d+) #{esc_gutter}/  # numbers in group $1
    end
    while ln = inf.gets
      ln = ln.chomp
      if options[:unnumber] == :gutter
        STDOUT.puts ln.gsub( pat, "#{$1} ")  # keep the digits+space, ditch the gutter
      else
        STDOUT.puts ln.gsub( pat, '' )       # either ditch the digits, or ditch both
      end
    end  # while
  end
rescue Errno::ENOENT => e
  fnf_error_exit( e, inputf )
end  # unnumber_lines

# === Main ===
options = { :fldwidth => '4',
            :gutter     => ' ',  # a space
            :backup     => nil,
            :format     => :text,
            :unnumber   => false,
            :noop       => nil,
            :verbose    => false,
            :debug      => DBGLVL0,
            :about      => false
          }

optparse = OptionParser.new { |opts|
  opts.on( "-f", "--fieldwidth=[1..6|C]",
           "Size of numbering-field in characters (default is 4);",
            "enter a field width either as an integer (1-to-6),",
            "or C[OUNTLINES] to use the line-count returned by `wc`" ) do |val|
     options[:fldwidth] = val
  end  # -f --fieldwidth
  opts.on( "-g", "--gutter='STRING'", String,
           "Gutter string (default is a single space ' ')" ) do |val|
     options[:gutter] = val
  end  # -g --gutter
  opts.on( "-F", "--format=TEXT|RTF", /TEXT|RTF/i,
           "Format for output file:",
           "TEXT (default, plain text) or RTF" ) do |val|
     options[:format] = val.downcase.to_sym
  end  # -F --format
  opts.on( "-b", "--backup",
           "Create backup file before updating source" ) do |val|
    options[:backup] = val
  end  # -b --backup
  opts.on( "-u", "--unnumber=[BOTH|NUMBERS|GUTTER]", /BOTH|NUMBERS|GUTTER/i,
           "Remove both numbers & gutter, numbers only, or gutter only:",
           "BOTH (default), NUMBERS, or GUTTER",
           "Note: to remove gutter characters,",
           "      specify --gutter='STRING' exactly" ) do |val|
     options[:unnumber] = val ? val.downcase.to_sym : :both
  end  # -u --unnumber
  opts.separator ""
  # --- Dry-Run option ---
  opts.on( "-n", "--noop", "--dryrun", "--test",
           "Dry-run (test & display, no-op) mode" ) do |val|
    options[:noop]  = true
    options[:verbose] = true  # Dry-run implies verbose...
  end  # -n --noop
  # --- Verbose option ---
  opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
    options[:verbose] = true
  end  # -v --verbose
  # --- Debug option ---
  opts.on( "-d", "--debug", "=DebugLevel", Integer,
           "Show debug information (levels: 1, 2 or 3)",
           "  1 - enables basic debugging information",
           "  2 - enables advanced debugging information",
           "  3 - enables (starts) pry-byebug debugger" ) do |val|
    options[:debug] = val.to_i
  end  # -d --debug
  # --- About option ---
  opts.on_tail( "-a", "--about", "Display program info" ) do |val|
    $stdout.puts "#{PROGID}"
    $stdout.puts "#{AUTHOR}"
    options[:about] = true
    exit true
  end  # -a --about
  # --- Set the banner & Help option ---
  opts.banner = "\n  Usage: #{PROGNAME} [options] [filename] [ >outfilename ]" +
                "\n\n    where optional filename is the name (or pathname) of a text file." +
                "\n\n    If filename is missing, StdIn is used (pipe-able)." +
                "\n\n  Note that output is to StdOut (pipe-able), but can be redirected" +
                "\n  to a file as needed (likely the normal use-case).\n\n"
  opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
    $stdout.puts opts
    # $stdout.puts "«+»Additional Text«+»"
    options[:help] = true
    exit true
  end  # -? --help
}.parse!  # leave residue-args in ARGV

###############################
if options[:debug] >= DBGLVL3 #
  require 'pry'               #
  binding.pry                 #
end                           #
###############################

pp options if options[ :debug ] >= DBGLVL2

tmpext  = '.tmp~'
bckext  = '.backup'

ARGV[0] = nil if ARGV.count == 0

ARGV.each do | inputf |
  if inputf               # here, nil means use StdIn, so no backup possible...
    if options[:backup]   # but only make the backup if requested...
      backupf = inputf + bckext
      FileUtils.cp( inputf, backupf )
    end
  end
  if options[:unnumber]
    unnumber_lines( inputf, options )
  else
    number_lines( inputf, options )
  end
  if backupf  # ...is not nil
    FileUtils.rm( backupf ) if File.extname( backupf ) == tmpext  # delete temp-file
  end
end
