#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# shortytext.rb
#
# Copyright © 2020-2024 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free4 Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

# Initially, for experimentation, bound to Alt+S
# using Settings widget, Keyboard tab, custom add "+"
# ShortyText Alt+S (/home/lorin/bin/shortytext)
#

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v2.1 (03/02/2024)"
  AUTHOR = "Lorin Ricker, Valley Center, California, USA"

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for binding.pry &/or pry-{byebug|nav} #
             ######################################################
# -----

###############################
if false                      #  <-- Enable/disable this manually, by edit...
  require 'pry'               #
  binding.pry                 #
end                           #
###############################

# This is a Ruby implementation derived (very respectfully) from Lee Blue's
# "texpander" utility (Version: 2.0, Release: November 24, 2017; see his
# project/repo: github.com/leeblue/texpander).
#
# See shortytext.rb_original for the first derivative version of this script
# which included Lee's original bash script with my Ruby cribbed-version.
#
# This variation removes the bash scripting comments, now focusing on advanced
# Ruby-enabled features.
#

pid = %x{ xdotool getwindowfocus getwindowpid }.strip
procname = %x{ cat /proc/#{pid}/comm }.strip

configdir = "#{ENV['HOME']}/.config/shortytext"
if !File.directory?( configdir )
  ## require 'fileutils'  ## Isn't this now part of Standard Library?
  FileUtils.mkdir_p( configdir, verbose: true )
end
basedir = File.realpath( configdir )

cmd = "shopt -s globstar ; find '#{basedir}' -type f | sort | sed 's?^#{basedir}/??g'"
abbrvs = %x{ #{cmd} }
# Zenity expects a filename "list" with files separated by spaces, not by \n (newlines)
abbrvs = abbrvs.split("\n").join(" ")

cmd = "zenity --list --title=#{PROGNAME} --width=275 --height=400 --column=Abbreviations #{abbrvs}"
name = %x{ #{cmd} }.strip
path = "#{basedir}/#{name}"

if File.file?( "#{basedir}/#{name}" )   # A regular file (not directory, etc)?
  if File.exist?( "#{path}" )           # and it really exists...
    clipboard = %x{ xsel -b -o }                # Save existing clipboard contents...
                                                # Put shortytext file's contents into
    %x{ echo -n "$(cat #{path})" | xsel -p -i } # primary buffer for Shift+Insert...
                                                # and into clipboard selection for
                                                # apps that use clipboard for pasting
    %x{ echo -n "$(cat #{path})" | xsel -b -i } # (like Firefox)...
    sleep 0.3
    %x{ xdotool key shift+Insert }
    # or...
    # %x{ xdotool type -- "$(xsel -bo | tr \\n \\r | sed s/\\r*\$//)" }
    sleep 0.5
    %x{ echo $clipboard | xsel -b -i }
  else
    %x{ zenity --error --text="%#{PROGNAME}-e-abbrnotfound, Abbreviation not found:\n#{name}" }
  end
end

exit true
