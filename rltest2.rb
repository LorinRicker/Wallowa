#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# rltest2.rb
#
# Copyright © 2021 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free4 Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

# This is a test/proof-of-concept, how to pre-seed Readline's input line buffer
# with pre-computed text.  The trick is in Readline's methods pre_input_hook
# and insert_text.  This trick/approach likely has limited use cases, but I've
# got one practical, immediate need...
# Found this approach here:
#    https://stackoverflow.com/questions/2314105/what-will-give-me-something-like-ruby-readline-with-a-default-value

require_relative 'lib/TerminalPrompted'

more = true
line = "Start with 01"

Readline.pre_input_hook = -> do
  Readline.insert_text( line )
  Readline.redisplay
end
# to remove the hook:  Readline.pre_input_hook = nil

while more
  line = getprompted( '>>> ', line )
  puts "echo: '#{line}'"
  line.succ!
end  # while

exit true
