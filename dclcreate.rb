#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# dclcreate.rb
#
# Copyright © 2019 Lorin Ricker <Lorin@RickerNet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free Software Foundation.
# See the file 'gpl' distributed within this project directory tree.

# Utility to take `touch` a step farther and
# imitate VMS DCL's CREATE file command.
#
#    This may ultimately replace the CREATE command in dcl.rb...
#

PROGNAME = File.basename $0
  PROGID = "#{PROGNAME} v1.3 (08/19/2019)"
  AUTHOR = "Lorin Ricker, Valley Center, California, USA"

SPC = ' '.freeze
USAGE = "\n  Usage: #{PROGNAME} [options] textfile".freeze

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for binding.pry &/or pry-{byebug|nav} #
             ######################################################

require 'optparse'

# -----

options = { :prompt   => nil,
            :verbose  => false,
            :debug    => DBGLVL0,
            :about    => false
          }

optparse = OptionParser.new { |opts|
  opts.on( "-p", "--prompt[=PSTRING]", "Prompt text (optional) for each data line;",
           "if this option is used without providing",
           "the PSTRING prompt-text, the default prompt",
           "text is 'data: '; terminate input with Ctrl/D" ) do |val|
    val += SPC if !val.nil? && val[val.length-1] != SPC  # not nil? add a trailing space if needed...
    options[:prompt] = val || 'data: '
  end  # -p --prompt
  opts.separator "\n"
  # --- Verbose option ---
  opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
    options[:verbose] = true
  end  # -v --verbose
  # --- Debug option ---
  opts.on( "-d", "--debug", "=DebugLevel", Integer,
           "Show debug information (levels: 1, 2 or 3)",
           "  1 - enables basic debugging information",
           "  2 - enables advanced debugging information",
           "  3 - enables (starts) pry-byebug debugger" ) do |val|
    options[:debug]   = val.to_i
    options[:verbose] = true
  end  # -d --debug
  # --- About option ---
  opts.on_tail( "-a", "--about", "Display program info" ) do |val|
    require_relative 'lib/AboutProgram'
    options[:about] = about_program( PROGID, AUTHOR, true )
  end  # -a --about
  # --- Set the banner & Help option ---
  opts.banner = USAGE +
                "\n\n    Creates a text file (similar to 'touch'), but provides for user input" +
                "\n    as line-by-line file contents (just like DCL's CREATE command).\n\n"
  opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
    $stdout.puts opts
    options[:help] = true
    exit true
  end  # -? --help
}.parse!  # leave residue-args in ARGV

###############################
if options[:debug] >= DBGLVL3 #
  require 'pry'               #
  binding.pry                 #
end                           #
###############################

pp options if options[:debug] >= DBGLVL1

if !ARGV[0]
  $stderr.puts USAGE
  exit false
end

begin
  File.open( ARGV[0], 'w', perm: 0o600, textmode: true ) do | outf |
    $stderr.puts "%#{PROGNAME}-i-create, copying 'stdin' to '#{ARGV[0]}'" if options[:verbose]
    $stdout.print options[:prompt] if options[:prompt]
    while !$stdin.eof?
      $stdout.print options[:prompt] if options[:prompt]
      outf.puts  $stdin.gets
    end  # while
  end
rescue SystemCallError => e
  # Expect "no file access or protection violation" issues here
  $stderr.puts "%#{PROGNAME}-e-fileaccess, cannot create '#{ARGV[0]}'"
  $stderr.puts "%#{PROGNAME}-e-rescued, #{e}", e.to_s
  exit false
end

exit true
