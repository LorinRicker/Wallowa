#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# linestats.rb
#
# Copyright © 2020 Lorin Ricker <lorin@rickernet.us>
# Version info: see PROGID below...
#
# This program is free software, under the terms and conditions of the
# GNU General Public License published by the Free4 Software Foundation.
# See the file 'gpl' distributed within this project directory tree.
#

# Use:  $ linestats [--options] textfile

# This tool simply displays as a graphic/histogram the length of lines
# in a text or sourcecode file.
#

PROGNAME = File.basename( $0 ).freeze
  PROGID = "#{PROGNAME} v0.1 (12/22/2020)".freeze
  AUTHOR = "Lorin Ricker, Valley Center, California, USA".freeze
  USAGE  = "\n  Usage: #{PROGNAME} [options] textfile".freeze

DBGLVL0 = 0
DBGLVL1 = 1
DBGLVL2 = 2  ######################################################
DBGLVL3 = 3  # <-- reserved for binding.pry &/or pry-{byebug|nav} #
             ######################################################

     BALLOTBOX = "\u2610"
HorizStrutUTF8 = "\u2500"
 VertStrutUTF8 = "\u2502"
  SWCornerUTF8 = "\u2514"
           SPC = ' '

# === Main ===

options = { :linenumbers => false,
            :histogram   => false,
            :noop        => false,
            :verbose     => false,
            :debug       => DBGLVL0,
            :about       => false
          }

# ----------

require 'optparse'
require_relative 'lib/CountLines'

optparse = OptionParser.new { |opts|
  # --- Display options ---
  opts.on( "-l", "--linenumbers",
           "Display line numbers too: [linenr,linesize]" ) do |val|
    options[:linenumbers] = 3  # default 3-digits field size
  end  # -l --linenumbers
  opts.on( "-g", "--histogram",
           "Display line-size histogram" ) do |val|
    options[:histogram] = true
  end  # -l --linenumbers

  opts.separator ""
  opts.on( "-n", "--noop", "--dryrun", "--test",
           "Dry-run (test & display, no-op) mode" ) do |val|
    options[:noop]  = true
    options[:verbose] = true  # Dry-run implies verbose...
  end  # -n --noop
  # --- Verbose option ---
  opts.on( "-v", "--verbose", "--log", "Verbose mode" ) do |val|
    options[:verbose] = true
  end  # -v --verbose
  # --- Debug option ---
  opts.on( "-d", "--debug", "=DebugLevel", Integer,
           "Show debug information (levels: 1, 2 or 3)",
           "  1 - enables basic debugging information",
           "  2 - enables advanced debugging information",
           "  3 - enables (starts) pry-byebug debugger" ) do |val|
    options[:debug] = val.to_i
  end  # -d --debug
  # --- About option ---
  opts.on_tail( "-a", "--about", "Display program info" ) do |val|
    require_relative 'lib/AboutProgram'
    options[:about] = about_program( PROGID, AUTHOR, true )
  end  # -a --about
  # --- Set the banner & Help option ---
  opts.banner = USAGE +
                "\n\n    where \"textfile\" is any file containing" +
                "\n    lines of printable text (ASCII, UTF-8, etc.)." +
                "\n\n    Displays "
  opts.on_tail( "-?", "-h", "--help", "Display this help text" ) do |val|
    $stdout.puts opts
    # $stdout.puts "«+»Additional Text«+»"
    options[:help] = true
    exit true
  end  # -? --help
}.parse!  # leave residue-args in ARGV

###############################
if options[:debug] >= DBGLVL3 #
  require 'pry'               #
  binding.pry                 #
end                           #
###############################

if ARGV.length == 0
  STDERR.puts USAGE
  exit false
end

srcfile = File.expand_path( ARGV[0] )
linecount, maxlen = File.count_lines( srcfile )
options[:linenumbers] = sprintf( "%d", linecount ).size if options[:linenumbers]
hist = Hash.new( 0 )

lctr = 0
HLINE = SWCornerUTF8 + ( HorizStrutUTF8 * maxlen )

File.open( srcfile, mode: 'r' ) do | f |
  STDOUT.puts ""
  while line = f.gets
    line = line.chomp
    lctr += 1
    llen  = line.length
    hist[llen] += 1
    mark  = options[:linenumbers] \
         ? sprintf( "[%*d,%3d]", options[:linenumbers], lctr, llen ) \
         : sprintf( "[%3d]", llen )
    mark += VertStrutUTF8
    marklen = mark.length
    mark = mark + ( BALLOTBOX * llen )
    STDOUT.puts mark
  end  # while
  indent = SPC * ( marklen - 1 )
  STDOUT.puts indent + HLINE
  STDOUT.puts indent + "Graph of Line Lengths (in file line order)"
end  # File.open

if options[:histogram]
  pp hist if options[:debug] >= DBGLVL3
  STDOUT.puts ""
  hist.sort.reverse.each do | key, valu |
    STDOUT.puts sprintf( "%3d %1s%s", key, VertStrutUTF8, BALLOTBOX * valu )
  end
  indent = SPC * 4
  STDOUT.puts indent + HLINE
  STDOUT.puts indent + "Histogram: Counts of Line Lengths (non-zero instances)"
end  # if

exit true
